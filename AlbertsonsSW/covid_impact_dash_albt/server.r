#
# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)
library(ggplot2)
library(dplyr)
library(RODBC)
library(tidyverse)
library(lubridate)
library(hrbrthemes)
library(shinythemes)
library(reshape)
library(reshape2)
library(DT)
library(patchwork)
library(scales)


# Define server logic required to draw a histogram
shinyServer(function(input, output) {
  
  plotData <- reactive({
    
    get_subset = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= '2019-12-01' 
             & week_ending_date <= actuals_max_date & mc_subsegment_c %in% (input$segment) & pack_group %in% (input$packgroup))
    
    get_subset = get_subset[ , c("week_ending_date", "dollars","forecasted_revenue","units","forecasted_units")]
    get_subset = get_subset %>%
      group_by(week_ending_date) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    # A few constants
    temperatureColor <- "#091F3F"
    #priceColor <- "#1496FF"
    priceColor <- "#FF6912"
    
    
    ggplot(get_subset, aes(x=week_ending_date)) +
      
      geom_line( aes(y=total_dollars), size=2, color=temperatureColor) + 
      geom_line( aes(y=total_forecasted_rev), size=2, color=priceColor) +
      
      scale_y_continuous(
        
        # Features of the first axis
        name = "Revenue"
        
        # Add a second axis and specify its features
        #   sec.axis = sec_axis(~.*coeff, name="Dollars")
      ) + 
      
      theme_ipsum() +
      
      theme(
        axis.title.y = element_text(color = temperatureColor, size=13)
        # axis.title.y.right = element_text(color = priceColor, size=13)
      ) +
      
      ggtitle(paste0("Total Revenue Impact for ", str_to_title(input$retailer_market)," Market-Level Geography"))
    
  })         
  
  output$rev_plot <- renderPlot(
    plotData()
    
  )
  
  plotData_units <- reactive({
    
    get_subset = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= '2019-12-01' 
             & week_ending_date <= actuals_max_date & mc_subsegment_c %in% (input$segment) & pack_group %in% (input$packgroup))
    
    get_subset = get_subset[ , c("week_ending_date", "dollars","forecasted_revenue","units","forecasted_units")]
    get_subset = get_subset %>%
      group_by(week_ending_date) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    # A few constants
    temperatureColor <- "#FF6912"
    priceColor <- "#091F3F"
    
    
    ggplot(get_subset, aes(x=week_ending_date)) +
      
      geom_line( aes(y=total_units), size=2, color=temperatureColor) + 
      geom_line( aes(y=total_forecasted_units), size=2, color=priceColor) +
      
      scale_y_continuous(
        
        # Features of the first axis
        name = "Units"
        
        # Add a second axis and specify its features
        #   sec.axis = sec_axis(~.*coeff, name="Dollars")
      ) + 
      
      theme_ipsum() +
      
      theme(
        axis.title.y = element_text(color = temperatureColor, size=13)
        # axis.title.y.right = element_text(color = priceColor, size=13)
      ) +
      
      ggtitle(paste0("Total Units Impact for ", str_to_title(input$retailer_market)," Market-Level Geography"))
    
  })         
  
  output$units_plot <- renderPlot(
    plotData_units()
    
  )
  
  
  tableData <- reactive({
    
    covid_stay_home_date = '2020-03-01'
    covid_stay_home_date = as.Date(covid_stay_home_date)
    
    table_data = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date & mc_subsegment_c %in% (input$segment) & pack_group %in% (input$packgroup))
    
    
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    table_data = table_data %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    ##get records from year ago during the same timeframe to index against last year's performace as well
    covid_stay_home_date_ya = covid_stay_home_date - 365
    actuals_max_date_ya = actuals_max_date - lubridate::days(365)
    
    table_data_ya = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= covid_stay_home_date_ya 
             & week_ending_date <= actuals_max_date_ya & mc_subsegment_c %in% input$segment & pack_group %in% (input$packgroup))
    
    table_data_ya = table_data_ya[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    table_data_ya = table_data_ya %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    table_data_ya = table_data_ya[ , c("mc_subsegment_c", "pack_group","total_dollars","total_units")]
    
    names(table_data_ya)[names(table_data_ya)=="total_dollars"] <- "revenue_ya"
    names(table_data_ya)[names(table_data_ya)=="total_units"] <- "units_ya"
    
    table_data <- merge(x =table_data,y =table_data_ya,by=c("mc_subsegment_c","pack_group"))
    
    #create indices against forecasts and year ago numbers for both Revenue and Units
    table_data$revenue_forecasts_index = scales::percent(round(table_data$total_dollars/table_data$total_forecasted_rev,2)-1)
    table_data$units_forecasts_index = scales::percent(round(table_data$total_units/table_data$total_forecasted_units,2)-1)
    
    table_data$revenue_forecasts_actuals = scales::dollar(round(table_data$total_dollars - table_data$total_forecasted_rev,2))
    table_data$units_forecasts_actuals = scales::comma(round(table_data$total_units - table_data$total_forecasted_units,2))
    
    table_data$revenue_ya_index = scales::percent(round(table_data$total_dollars/table_data$revenue_ya,2)-1)
    table_data$units_ya_index = scales::percent(round(table_data$total_units/table_data$units_ya,2)-1)
    
    table_data$revenue_ya_actuals = scales::dollar(round(table_data$total_dollars - table_data$revenue_ya,2))
    table_data$units_ya_actuals = scales::comma(round(table_data$total_units - table_data$units_ya,2))
    
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","revenue_forecasts_index","units_forecasts_index","revenue_forecasts_actuals","units_forecasts_actuals","revenue_ya_index","units_ya_index","revenue_ya_actuals","units_ya_actuals")]
    table_data <- as.data.frame(table_data)
    table_data = melt(table_data, id.vars = c("mc_subsegment_c", "pack_group"), measure.vars = c("revenue_forecasts_index","units_forecasts_index","revenue_forecasts_actuals","units_forecasts_actuals","revenue_ya_index","units_ya_index","revenue_ya_actuals","units_ya_actuals"))
    table_data$covid_index = table_data$value
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","covid_index","variable")]
    table_data$index_value = table_data$variable
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","covid_index","index_value")]
    table_data = dcast(table_data, mc_subsegment_c + index_value ~ pack_group, value.var = "covid_index" )
    names(table_data)[names(table_data)=="mc_subsegment_c"] <- "Segment"
    names(table_data)[names(table_data)=="index_value"] <- "Index"
    #             names(table_data)[names(table_data)=="Twelve_to_Twenty_Packs"] <- "12pks to 24pks"
    #             names(table_data)[names(table_data)=="TwentyFour_to_36_Packs"] <- "24pks to 36pks"
    #             names(table_data)[names(table_data)=="Two_to_Ten_Packs"] <- "2pks to 10pks"
    table_data$Segment = str_to_title(table_data$Segment)
    column_list = c('Index', 'Segment', input$packgroup)
    table_data = table_data[,c(column_list)]
    
    return(table_data)
  })
  
  output$tbl <- renderDataTable(
    
    tableData() %>%
      filter(Index == input$rev_or_units_index) %>%
      select(c('Segment', input$packgroup))
  )
  
  
  
  
  
  stayHome_txt <- reactive({
    
    covid_stay_home_date = '2020-03-01'
    covid_stay_home_date = as.Date(covid_stay_home_date)
    
    
    return(covid_stay_home_date)
  })
  
  
  output$sayhomestartTxt = renderText(
    paste0("Stay Home Start Date: ", stayHome_txt())
  )
  
  totalUnits_txt <- reactive({
    
    covid_stay_home_date = '2020-03-01'
    covid_stay_home_date = as.Date(covid_stay_home_date)
    
    
    ttlunitstxt = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date)
    
    total_units_actual = sum(ttlunitstxt$units)
    total_units_forecasted = sum(ttlunitstxt$forecasted_units)
    
    total_units_text = scales::percent(round(total_units_actual/total_units_forecasted,2) - 1) 
    return(total_units_text)
  })
  
  
  output$totalUnitsTxt = renderText(
    paste0("Total Units Index: ", totalUnits_txt())
  )
  
  totalRev_txt <- reactive({
    
    covid_stay_home_date = '2020-03-01'
    covid_stay_home_date = as.Date(covid_stay_home_date)
    
    ttlrevtxt = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date)
    
    total_rev_actual = sum(ttlrevtxt$dollars)
    total_rev_forecasted = sum(ttlrevtxt$forecasted_revenue)
    
    total_rev_text =  scales::percent(round(total_rev_actual/total_rev_forecasted,2) - 1)
    return(total_rev_text)
  })
  
  
  output$totalRevTxt = renderText(
    paste0("Total Revenue Index: ", totalRev_txt())
  )
  
  totalUnitsActuals_txt <- reactive({
    
    covid_stay_home_date = '2020-03-01'
    covid_stay_home_date = as.Date(covid_stay_home_date)
    
    
    ttlunitstxt = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date)
    
    total_units_actual = sum(ttlunitstxt$units)
    total_units_forecasted = sum(ttlunitstxt$forecasted_units)
    
    total_units_actuals_text = scales::comma(round(total_units_actual-total_units_forecasted,2))
    return(total_units_actuals_text)
  })
  
  
  output$totalUnitsActualsTxt = renderText(
    paste0("Total Units Impact: ", totalUnitsActuals_txt())
  )
  
  totalRevActuals_txt <- reactive({
    
    covid_stay_home_date = '2020-03-01'
    covid_stay_home_date = as.Date(covid_stay_home_date)
    
    ttlrevtxt = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date)
    
    total_rev_actual = sum(ttlrevtxt$dollars)
    total_rev_forecasted = sum(ttlrevtxt$forecasted_revenue)
    
    total_rev_actuls_text = scales::dollar(round(total_rev_actual - total_rev_forecasted,2))
    return(total_rev_actuls_text)
  })
  
  
  output$totalRevActualsTxt = renderText(
    paste0("Total Revenue Impact: ",  totalRevActuals_txt())
  )
  
  
  COVIDtrendtableData <- reactive({
    trend_data = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= (actuals_max_date - as.difftime(28, unit="days")) 
             & week_ending_date <= actuals_max_date)
    
    trend_data = trend_data[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    trend_data = trend_data %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    ##get records from year ago during the same timeframe to index against last year's performace as well
    actuals_max_date_ya = actuals_max_date - lubridate::days(365)
    actuals_max_date_4weeksprior_ya = actuals_max_date - lubridate::days(393)
    
    trend_data_ya = total_data %>%
      filter( market_display_name_mlr == input$retailer_market & week_ending_date >= actuals_max_date_4weeksprior_ya 
              & week_ending_date <= actuals_max_date_ya)
    
    trend_data_ya = trend_data_ya[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    trend_data_ya = trend_data_ya %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    trend_data_ya = trend_data_ya[ , c("mc_subsegment_c", "pack_group","total_dollars","total_units")]
    
    names(trend_data_ya)[names(trend_data_ya)=="total_dollars"] <- "revenue_ya"
    names(trend_data_ya)[names(trend_data_ya)=="total_units"] <- "units_ya"
    
    trend_data_total <- merge(x =trend_data,y =trend_data_ya,by=c("mc_subsegment_c","pack_group"))
    
    #change 0 values to 0.1 to avoid divide by zero errors
    trend_data_total[trend_data_total=="0"]<-0.1
    
    #create indices against forecasts and year ago numbers for both Revenue and Units
    trend_data_total$revenue_forecasts_index = round(trend_data_total$total_dollars/trend_data_total$total_forecasted_rev,2)
    trend_data_total$units_forecasts_index = round(trend_data_total$total_units/trend_data_total$total_forecasted_units,2)
    
    trend_data_total$revenue_forecasts_actuals = scales::dollar(round(trend_data_total$total_dollars - trend_data_total$total_forecasted_rev,2))
    trend_data_total$units_forecasts_actuals = scales::comma(round(trend_data_total$total_units - trend_data_total$total_forecasted_units,2))
    
    trend_data_total$revenue_ya_index = round(trend_data_total$total_dollars/trend_data_total$revenue_ya,2)
    trend_data_total$units_ya_index = round(trend_data_total$total_units/trend_data_total$units_ya,2)
    
    trend_data_total$revenue_ya_actuals = scales::dollar(round(trend_data_total$total_dollars - trend_data_total$revenue_ya,2))
    trend_data_total$units_ya_actuals = scales::comma(round(trend_data_total$total_units - trend_data_total$units_ya,2))
    
    trend_data_total = trend_data_total[ , c("mc_subsegment_c", "pack_group","revenue_forecasts_index","units_forecasts_index","revenue_ya_index","units_ya_index")]
    trend_data_total <- as.data.frame(trend_data_total)
    
    
    #merge onto forecasts for future trend creations
    trend_data_total_90 <- merge(x =trend_data_total,y =forecast_data_90,by=c("mc_subsegment_c","pack_group"))
    trend_data_total_180 <- merge(x =trend_data_total,y =forecast_data_180,by=c("mc_subsegment_c","pack_group"))
    
    trend_data_total <- rbind(trend_data_total_90,trend_data_total_180)
    
    trend_data_total = trend_data_total[ , c("mc_subsegment_c", "pack_group","revenue_forecasts_index","units_forecasts_index","revenue_ya_index","units_ya_index","market_display_name_mlr","total_forecasted_units","total_forecasted_rev","forecast_window")]
    
    trend_data_total$covid_trend_rev = trend_data_total$revenue_forecasts_index*trend_data_total$total_forecasted_rev
    trend_data_total$covid_trend_units = trend_data_total$units_forecasts_index*trend_data_total$total_forecasted_units
    
    #if (input$future_level == "Total Geo/Retailer") {
    if (input$future_level == "Total Geo/Retailer") {
      covid_forecasts = trend_data_total[ , c("revenue_forecasts_index","units_forecasts_index","covid_trend_units","covid_trend_rev","forecast_window")]
      covid_forecasts = covid_forecasts %>%
        group_by(forecast_window) %>%
        summarise(avg_revenue_forecasts_index = mean(revenue_forecasts_index), avg_units_forecasts_index = mean(units_forecasts_index), forecasted_units = sum(covid_trend_units), forecasted_rev = sum(covid_trend_rev))
      covid_forecasts = covid_forecasts %>%
        filter(forecast_window == forecast_timeframe_test)
    } else if (input$future_level == "Segment") {
      covid_forecasts = trend_data_total[ , c("mc_subsegment_c","revenue_forecasts_index","units_forecasts_index","covid_trend_units","covid_trend_rev","forecast_window")]
      covid_forecasts = covid_forecasts %>%
        group_by(mc_subsegment_c,forecast_window) %>%
        summarise(avg_revenue_forecasts_index = mean(revenue_forecasts_index), avg_units_forecasts_index = mean(units_forecasts_index), forecasted_units = sum(covid_trend_units), forecasted_rev = sum(covid_trend_rev))
      covid_forecasts = covid_forecasts %>%
        filter(forecast_window == forecast_timeframe_test)
    } else {
      covid_forecasts = trend_data_total[ , c("mc_subsegment_c","pack_group","revenue_forecasts_index","units_forecasts_index","covid_trend_units","covid_trend_rev","forecast_window")]
      covid_forecasts = covid_forecasts %>%
        group_by(mc_subsegment_c,pack_group,forecast_window) %>%
        summarise(avg_revenue_forecasts_index = mean(revenue_forecasts_index), avg_units_forecasts_index = mean(units_forecasts_index), forecasted_units = sum(covid_trend_units), forecasted_rev = sum(covid_trend_rev))
      covid_forecasts = covid_forecasts %>%
        filter(forecast_window == forecast_timeframe_test)
    }   
    
    return(covid_forecasts)
  })
  
  output$covid_trend_tbl <- renderDataTable(
    
    COVIDtrendtableData()
  )
  
  PreCOVIDtrendtableData <- reactive({
    trend_data = total_data %>%
      filter(market_display_name_mlr == input$retailer_market & week_ending_date >= (actuals_max_date - as.difftime(28, unit="days")) 
             & week_ending_date <= actuals_max_date)
    
    trend_data = trend_data[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    trend_data = trend_data %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    ##get records from year ago during the same timeframe to index against last year's performace as well
    actuals_max_date_ya = actuals_max_date - lubridate::days(365)
    actuals_max_date_4weeksprior_ya = actuals_max_date - lubridate::days(393)
    
    trend_data_ya = total_data %>%
      filter( market_display_name_mlr == input$retailer_market & week_ending_date >= actuals_max_date_4weeksprior_ya 
              & week_ending_date <= actuals_max_date_ya)
    
    trend_data_ya = trend_data_ya[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    trend_data_ya = trend_data_ya %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    trend_data_ya = trend_data_ya[ , c("mc_subsegment_c", "pack_group","total_dollars","total_units")]
    
    names(trend_data_ya)[names(trend_data_ya)=="total_dollars"] <- "revenue_ya"
    names(trend_data_ya)[names(trend_data_ya)=="total_units"] <- "units_ya"
    
    trend_data_total <- merge(x =trend_data,y =trend_data_ya,by=c("mc_subsegment_c","pack_group"))
    
    #change 0 values to 0.1 to avoid divide by zero errors
    trend_data_total[trend_data_total=="0"]<-0.1
    
    #create indices against forecasts and year ago numbers for both Revenue and Units
    trend_data_total$revenue_forecasts_index = round(trend_data_total$total_dollars/trend_data_total$total_forecasted_rev,2)
    trend_data_total$units_forecasts_index = round(trend_data_total$total_units/trend_data_total$total_forecasted_units,2)
    
    trend_data_total$revenue_forecasts_actuals = scales::dollar(round(trend_data_total$total_dollars - trend_data_total$total_forecasted_rev,2))
    trend_data_total$units_forecasts_actuals = scales::comma(round(trend_data_total$total_units - trend_data_total$total_forecasted_units,2))
    
    trend_data_total$revenue_ya_index = round(trend_data_total$total_dollars/trend_data_total$revenue_ya,2)
    trend_data_total$units_ya_index = round(trend_data_total$total_units/trend_data_total$units_ya,2)
    
    trend_data_total$revenue_ya_actuals = scales::dollar(round(trend_data_total$total_dollars - trend_data_total$revenue_ya,2))
    trend_data_total$units_ya_actuals = scales::comma(round(trend_data_total$total_units - trend_data_total$units_ya,2))
    
    trend_data_total = trend_data_total[ , c("mc_subsegment_c", "pack_group","revenue_forecasts_index","units_forecasts_index","revenue_ya_index","units_ya_index")]
    trend_data_total <- as.data.frame(trend_data_total)
    
    
    #merge onto forecasts for future trend creations
    trend_data_total_90 <- merge(x =trend_data_total,y =forecast_data_90,by=c("mc_subsegment_c","pack_group"))
    trend_data_total_180 <- merge(x =trend_data_total,y =forecast_data_180,by=c("mc_subsegment_c","pack_group"))
    
    trend_data_total <- rbind(trend_data_total_90,trend_data_total_180)
    
    trend_data_total = trend_data_total[ , c("mc_subsegment_c", "pack_group","revenue_forecasts_index","units_forecasts_index","revenue_ya_index","units_ya_index","market_display_name_mlr","total_forecasted_units","total_forecasted_rev","forecast_window")]
    
    #if (input$future_level == "Total Geo/Retailer") {
    if (input$future_level == "Total Geo/Retailer") {
      precovid_forecasts = trend_data_total[ , c("revenue_forecasts_index","units_forecasts_index","total_forecasted_units","total_forecasted_rev","forecast_window")]
      precovid_forecasts = precovid_forecasts %>%
        group_by(forecast_window) %>%
        summarise(avg_revenue_forecasts_index = mean(revenue_forecasts_index), avg_units_forecasts_index = mean(units_forecasts_index), forecasted_units = sum(total_forecasted_units), forecasted_rev = sum(total_forecasted_rev))
      precovid_forecasts = precovid_forecasts %>%
        filter(forecast_window == forecast_timeframe_test)
    } else if (input$future_level == "Segment") {
      precovid_forecasts = trend_data_total[ , c("mc_subsegment_c","revenue_forecasts_index","units_forecasts_index","total_forecasted_units","total_forecasted_rev","forecast_window")]
      precovid_forecasts = precovid_forecasts %>%
        group_by(mc_subsegment_c,forecast_window) %>%
        summarise(avg_revenue_forecasts_index = mean(revenue_forecasts_index), avg_units_forecasts_index = mean(units_forecasts_index), forecasted_units = sum(total_forecasted_units), forecasted_rev = sum(total_forecasted_rev))
      precovid_forecasts = precovid_forecasts %>%
        filter(forecast_window == forecast_timeframe_test)
    } else {
      precovid_forecasts = trend_data_total[ , c("mc_subsegment_c","pack_group","revenue_forecasts_index","units_forecasts_index","total_forecasted_units","total_forecasted_rev","forecast_window")]
      precovid_forecasts = precovid_forecasts %>%
        group_by(mc_subsegment_c,pack_group,forecast_window) %>%
        summarise(avg_revenue_forecasts_index = mean(revenue_forecasts_index), avg_units_forecasts_index = mean(units_forecasts_index), forecasted_units = sum(total_forecasted_units), forecasted_rev = sum(total_forecasted_rev))
      precovid_forecasts = precovid_forecasts %>%
        filter(forecast_window == forecast_timeframe_test)
    }   
    
    return(precovid_forecasts)
  })
  
  output$precovid_trend_tbl <- renderDataTable(
    
    PreCOVIDtrendtableData()
  )
  
})

