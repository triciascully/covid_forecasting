library(shiny,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(ggplot2,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(dplyr,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(RODBC,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(tidyverse,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(lubridate,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(hrbrthemes,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(shinythemes,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(reshape,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(reshape2,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(DT,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")
library(patchwork,lib.loc="/opt/microsoft/ropen/3.4.2/lib64/R/library")

#
database = 'patricia_scully'
conn = odbcDriverConnect(paste0("DSN=impala;Database=",database), case="nochange")
#
## Bring in the three datasets
forecasts = sqlQuery(conn, paste("SELECT * FROM ", database,".covid_forecasts"),
                     stringsAsFactors = FALSE)
#
forecasts$week_ending_date <- as.POSIXct(forecasts$week_ending_date,format="%Y-%m-%d")
forecasts$forecasted_units <- as.numeric(forecasts$forecasted_units)
forecasts$forecasted_revenue <- as.numeric(forecasts$forecasted_revenue)
forecasts$cv_score <- as.numeric(forecasts$cv_score)
forecasts = forecasts[ , c("week_ending_date", "store_name","state","mc_subsegment_c","pack_group","model_group","forecasted_units","forecasted_revenue","cv_score")]

historicals = sqlQuery(conn, paste("SELECT * FROM ", database,".covid_historicals"),
                       stringsAsFactors = FALSE)

historicals$week_ending_date <- as.POSIXct(historicals$week_ending_date,format="%Y-%m-%d")
# historicals = historicals %>%
#     filter(store_name == 'circle k' & state == 'az' & week_ending_date > '2020-02-01' & week_ending_date <= actuals_max_date)
historicals$forecasted_units <- as.numeric(historicals$forecasted_units)
historicals$forecasted_revenue <- as.numeric(historicals$forecasted_revenue)
historicals$units <- as.numeric(historicals$units)
historicals$dollars <- as.numeric(historicals$dollars)
historicals$cv_score <- NA
historicals$cv_score <- as.numeric(historicals$cv_score)


actuals = sqlQuery(conn, paste("SELECT * FROM ", database,".covid_actuals"),
                   stringsAsFactors = FALSE)

actuals$week_ending_date <- as.POSIXct(actuals$week_ending_date,format="%Y-%m-%d")
actuals$units <- as.numeric(actuals$units)
actuals$dollars <- as.numeric(actuals$dollars)

#grab min and max dates from actuals data (for later)
actuals_min_date = min(actuals$week_ending_date)
actuals_max_date = max(actuals$week_ending_date)

#merge forecasts and actuals data
forecasts_and_actuals <- merge(x =forecasts,y =actuals,by=c("week_ending_date","model_group"), all.y = TRUE)

#append historical data onto forecasts and actuals
total_data <- rbind(forecasts_and_actuals, historicals)

total_data = filter(total_data, !is.na(state) | state != "")

covid_dates = sqlQuery(conn, paste("SELECT * FROM ", database,".covid_dates_by_state"),
                       stringsAsFactors = FALSE)

#covid_dates$stay_home_start_date <- as.POSIXct(covid_dates$stay_home_start_date,format="%m-%d-%Y")

# Define UI for application that draws a histogram
ui <-   tagList(
  tags$head(tags$script(type="text/javascript", src = "code.js")),
  navbarPage(title = "COVID-19 Dashboard",
             fluid = TRUE, 
             theme = shinytheme("united"), #flatly, superhero, united, yeti, slate
             tabPanel("COVID-19 Impact",
                      fluidPage(
                        sidebarLayout(
                          sidebarPanel(
                            selectInput("retailer", 
                                        label = h4("Select Retailer"),
                                        choices = unique(total_data$store_name)
                                        # selected = unique(data$store_name)
                            ),
                            selectInput("state",
                                        label = h4("Select State"),
                                        choices = unique(total_data$state),
                                        # selected = unique(data$state)
                            ),
                            
                            checkboxGroupInput("segment",
                                               label = h4("Select Segment"),
                                               choices = unique(total_data$mc_subsegment_c),
                                               selected = unique(total_data$mc_subsegment_c)
                            ),
                            
                            checkboxGroupInput("packgroup",
                                               label = h4("Select Pack Group"),
                                               choices = unique(total_data$pack_group),
                                               selected = unique(total_data$pack_group)
                            ),
                            #   submitButton()
                          ),
                          # Show a plot of the generated distribution
                          mainPanel(
                            plotOutput("rev_plot"),
                            plotOutput("units_plot")
                            
                          )
                        )
                        ,
                        # valueBoxes
                        fluidRow(
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("sayhomestartTxt"),
                                   tags$head(tags$style("#sayhomestartTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 )),
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("totalRevTxt"),
                                   tags$head(tags$style("#totalRevTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 )),
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("totalUnitsTxt"),
                                   tags$head(tags$style("#totalUnitsTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 ))),
                        fluidRow(
                          column(width = 4,
                                 wellPanel(  radioButtons("rev_or_units_index", label = h4("Select Index:"),
                                                          choices = list("Revenue/Forecasts" = "revenue_forecasts_index",
                                                                         "Units/Forecasts" = "units_forecasts_index",
                                                                         "Revenue/Revenue Year Ago" = "revenue_ya_index",
                                                                         "Units/Units Year Ago"="units_ya_index"), 
                                                          selected = "revenue_forecasts_index")
                                 ) 
                          ),
                          
                          column(width = 8,
                                 DTOutput("tbl")  
                          )
                          
                        )
                      )
             )
             ,
             
             tabPanel("Scenario Planner",
                      fluidPage(
                      )
             )
  )
)


# Define server logic required to draw a histogram
server <- function(input, output) {
  
  plotData <- reactive({
    
    get_subset = total_data %>%
      filter(store_name == input$retailer & state == input$state & week_ending_date >= '2019-12-01' 
             & week_ending_date <= actuals_max_date & mc_subsegment_c %in% (input$segment) & pack_group %in% (input$packgroup))
    
    get_subset = get_subset[ , c("week_ending_date", "dollars","forecasted_revenue","units","forecasted_units")]
    get_subset = get_subset %>%
      group_by(week_ending_date) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    # A few constants
    temperatureColor <- "#091F3F"
    #priceColor <- "#1496FF"
    priceColor <- "#FF6912"
    
    
    ggplot(get_subset, aes(x=week_ending_date)) +
      
      geom_line( aes(y=total_dollars), size=2, color=temperatureColor) + 
      geom_line( aes(y=total_forecasted_rev), size=2, color=priceColor) +
      
      scale_y_continuous(
        
        # Features of the first axis
        name = "Revenue",
        
        # Add a second axis and specify its features
        #   sec.axis = sec_axis(~.*coeff, name="Dollars")
      ) + 
      
      theme_ipsum() +
      
      theme(
        axis.title.y = element_text(color = temperatureColor, size=13),
        # axis.title.y.right = element_text(color = priceColor, size=13)
      ) +
      
      ggtitle(paste0("Total Revenue Impact for ", str_to_title(input$retailer)," stores in ", str_to_upper(input$state)))
    
  })         
  
  output$rev_plot <- renderPlot(
    plotData()
    
  )
  
  plotData_units <- reactive({
    
    get_subset = total_data %>%
      filter(store_name == input$retailer & state == input$state & week_ending_date >= '2019-12-01' 
             & week_ending_date <= actuals_max_date & mc_subsegment_c %in% (input$segment) & pack_group %in% (input$packgroup))
    
    get_subset = get_subset[ , c("week_ending_date", "dollars","forecasted_revenue","units","forecasted_units")]
    get_subset = get_subset %>%
      group_by(week_ending_date) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    # A few constants
    temperatureColor <- "#FF6912"
    priceColor <- "#091F3F"
    
    
    ggplot(get_subset, aes(x=week_ending_date)) +
      
      geom_line( aes(y=total_units), size=2, color=temperatureColor) + 
      geom_line( aes(y=total_forecasted_units), size=2, color=priceColor) +
      
      scale_y_continuous(
        
        # Features of the first axis
        name = "Units",
        
        # Add a second axis and specify its features
        #   sec.axis = sec_axis(~.*coeff, name="Dollars")
      ) + 
      
      theme_ipsum() +
      
      theme(
        axis.title.y = element_text(color = temperatureColor, size=13),
        # axis.title.y.right = element_text(color = priceColor, size=13)
      ) +
      
      ggtitle(paste0("Total Units Impact for ", str_to_title(input$retailer)," stores in ", str_to_upper(input$state)))
    
  })         
  
  output$units_plot <- renderPlot(
    plotData_units()
    
  )
  
  
  tableData <- reactive({
    
    covid_stay_home_date = covid_dates %>%
      filter(state == input$state)
    
    covid_stay_home_date = covid_stay_home_date[ ,c("stay_home_start_date")]
    covid_stay_home_date = as.Date(covid_stay_home_date, "%m/%d/%Y")
    
    table_data = total_data %>%
      filter(store_name == input$retailer & state == input$state & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date & mc_subsegment_c %in% (input$segment) & pack_group %in% (input$packgroup))
    
    
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    table_data = table_data %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    ##get records from year ago during the same timeframe to index against last year's performace as well
    covid_stay_home_date_ya = covid_stay_home_date - 365
    actuals_max_date_ya = actuals_max_date - lubridate::days(365)
    
    table_data_ya = total_data %>%
      filter(store_name == input$retailer & state == input$state & week_ending_date >= covid_stay_home_date_ya 
             & week_ending_date <= actuals_max_date_ya & mc_subsegment_c %in% input$segment & pack_group %in% (input$packgroup))
    
    table_data_ya = table_data_ya[ , c("mc_subsegment_c", "pack_group","forecasted_units","forecasted_revenue","units","dollars")]
    table_data_ya = table_data_ya %>%
      group_by(mc_subsegment_c, pack_group) %>%
      summarise(total_units = sum(units), total_forecasted_units = sum(forecasted_units), total_dollars = sum(dollars), total_forecasted_rev = sum(forecasted_revenue))
    
    table_data_ya = table_data_ya[ , c("mc_subsegment_c", "pack_group","total_dollars","total_units")]
    
    names(table_data_ya)[names(table_data_ya)=="total_dollars"] <- "revenue_ya"
    names(table_data_ya)[names(table_data_ya)=="total_units"] <- "units_ya"
    
    table_data <- merge(x =table_data,y =table_data_ya,by=c("mc_subsegment_c","pack_group"))
    
    #create indices against forecasts and year ago numbers for both Revenue and Units
    table_data$revenue_forecasts_index = round(table_data$total_dollars/table_data$total_forecasted_rev,2)
    table_data$units_forecasts_index = round(table_data$total_units/table_data$total_forecasted_units,2)
    
    table_data$revenue_ya_index = round(table_data$total_dollars/table_data$revenue_ya,2)
    table_data$units_ya_index = round(table_data$total_units/table_data$units_ya,2)
    
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","revenue_forecasts_index","units_forecasts_index","revenue_ya_index","units_ya_index")]
    table_data <- as.data.frame(table_data)
    table_data = melt(table_data, id.vars = c("mc_subsegment_c", "pack_group"), measure.vars = c("revenue_forecasts_index","units_forecasts_index","revenue_ya_index","units_ya_index"))
    table_data$covid_index = table_data$value
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","covid_index","variable")]
    table_data$index_value = table_data$variable
    table_data = table_data[ , c("mc_subsegment_c", "pack_group","covid_index","index_value")]
    table_data = dcast(table_data, mc_subsegment_c + index_value ~ pack_group, value.var = "covid_index" )
    
    return(table_data)
  })
  
  output$tbl <- renderDataTable(
    tableData() %>%
      filter(index_value == input$rev_or_units_index)
  )
  
  stayHome_txt <- reactive({
    
    covid_stay_home_date = covid_dates %>%
      filter(state == input$state)
    
    covid_stay_home_date = covid_stay_home_date[ ,c("stay_home_start_date")]
    
    covid_stay_home_date = as.Date(covid_stay_home_date, "%m/%d/%Y")
    
    return(covid_stay_home_date)
  })
  
  
  output$sayhomestartTxt = renderText(
    paste0("Stay Home Start Date: ", stayHome_txt())
  )
  
  totalUnits_txt <- reactive({
    
    covid_stay_home_date = covid_dates %>%
      filter(state == input$state)
    
    covid_stay_home_date = covid_stay_home_date[ ,c("stay_home_start_date")]
    
    covid_stay_home_date = as.Date(covid_stay_home_date, "%m/%d/%Y")
    
    ttlunitstxt = total_data %>%
      filter(store_name == input$retailer & state == input$state & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date)
    
    total_units_actual = sum(ttlunitstxt$units)
    total_units_forecasted = sum(ttlunitstxt$forecasted_units)
    
    total_units_text = round(total_units_actual/total_units_forecasted,2)
    return(total_units_text)
  })
  
  
  output$totalUnitsTxt = renderText(
    paste0("Total Units Index: ", totalUnits_txt())
  )
  
  totalRev_txt <- reactive({
    
    covid_stay_home_date = covid_dates %>%
      filter(state == input$state)
    
    covid_stay_home_date = covid_stay_home_date[ ,c("stay_home_start_date")]
    
    covid_stay_home_date = as.Date(covid_stay_home_date, "%m/%d/%Y")
    
    ttlrevtxt = total_data %>%
      filter(store_name == input$retailer & state == input$state & week_ending_date >= covid_stay_home_date 
             & week_ending_date <= actuals_max_date)
    
    total_rev_actual = sum(ttlrevtxt$dollars)
    total_rev_forecasted = sum(ttlrevtxt$forecasted_revenue)
    
    total_rev_text = round(total_rev_actual/total_rev_forecasted,2)
    return(total_rev_text)
  })
  
  
  output$totalRevTxt = renderText(
    paste0("Total Revenue Index: ", totalRev_txt())
  )
}

# Run the application 
shinyApp(ui = ui, server = server)



