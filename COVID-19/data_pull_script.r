library(shiny)
library(ggplot2)
library(dplyr)
library(RODBC)
library(tidyverse)
library(lubridate)
library(hrbrthemes)
library(shinythemes)
library(reshape)
library(reshape2)
library(DT)
library(patchwork)
library(scales)


#
database = 'patricia_scully'
conn = odbcDriverConnect(paste0("DSN=impala;Database=",database), case="nochange")
#
## Bring in the three datasets
forecasts = sqlQuery(conn, paste("SELECT * FROM ", database,".covid_forecasts_albertsons2"),
                     stringsAsFactors = FALSE)
#
# forecasts = forecasts %>%
#     filter(store_name == 'circle k')
#
forecasts$week_ending_date <- as.POSIXct(forecasts$week_ending_date,format="%Y-%m-%d")
forecasts$forecasted_units <- as.numeric(forecasts$forecasted_units)
forecasts$forecasted_revenue <- as.numeric(forecasts$forecasted_revenue)
forecasts$cv_score <- as.numeric(forecasts$cv_score)
forecasts = forecasts[ , c("week_ending_date", "market_display_name_mlr","mc_subsegment_c","pack_group","model_group","forecasted_units","forecasted_revenue","cv_score")]

historicals = sqlQuery(conn, paste("SELECT DISTINCT * FROM ", database,".covid_historicals_albertsons"),
                       stringsAsFactors = FALSE)

##  historicals = historicals %>%
#      filter(store_name == 'circle k')

historicals$week_ending_date <- as.POSIXct(historicals$week_ending_date,format="%Y-%m-%d")
# historicals = historicals %>%
#     filter(store_name == 'circle k' & state == 'az' & week_ending_date > '2020-02-01' & week_ending_date <= actuals_max_date)
historicals$forecasted_units <- as.numeric(historicals$forecasted_units)
historicals$forecasted_revenue <- as.numeric(historicals$forecasted_revenue)
historicals$units <- as.numeric(historicals$units)
historicals$dollars <- as.numeric(historicals$dollars)
historicals$cv_score <- NA
historicals$cv_score <- as.numeric(historicals$cv_score)


actuals = sqlQuery(conn, paste("SELECT * FROM ", database,".covid_actuals_albertsons"),
                   stringsAsFactors = FALSE)

#  actuals = actuals %>%
#      filter(store_name == 'circle k')

actuals$week_ending_date <- as.POSIXct(actuals$week_ending_date,format="%Y-%m-%d")
actuals$units <- as.numeric(actuals$units)
actuals$dollars <- as.numeric(actuals$dollars)

#grab min and max dates from actuals data (for later)
actuals_min_date = min(actuals$week_ending_date)
actuals_max_date = max(actuals$week_ending_date)

#merge forecasts and actuals data
forecasts_and_actuals <- merge(x =forecasts,y =actuals,by=c("week_ending_date","model_group"), all.y = TRUE)

#   forecasts_and_actuals = forecasts_and_actuals %>%
#       filter(store_name == 'circle k')

#append historical data onto forecasts and actuals
total_data <- rbind(forecasts_and_actuals, historicals)

total_data = filter(total_data, !is.na(market_display_name_mlr) | market_display_name_mlr != "")