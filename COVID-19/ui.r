#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)
library(ggplot2)
library(dplyr)
library(RODBC)
library(tidyverse)
library(lubridate)
library(hrbrthemes)
library(shinythemes)
library(reshape)
library(reshape2)
library(DT)
library(patchwork)
library(scales)


source("/data/home/patricia.scully/test3/data_pull_script.R")

# Define UI for application that draws a histogram
shinyUI( tagList(
  tags$head(tags$script(type="text/javascript", src = "code.js")),
  navbarPage(title = "COVID-19 Dashboard",
             fluid = TRUE, 
             theme = shinytheme("flatly"), #flatly, superhero, united, yeti, slate
             tabPanel("COVID-19 Impact",
                      fluidPage(
                        sidebarLayout(
                          sidebarPanel(
                            selectInput("retailer_market", 
                                        label = h4("Select Retailer Market"),
                                        choices = unique(total_data$market_display_name_mlr)
                                        # selected = unique(data$store_name)
                            ),
                            
                            checkboxGroupInput("segment",
                                               label = h4("Select Segment"),
                                               choices = list("Budget" = "budget",
                                                              "Cider" = "cider",
                                                              "Coolers, FMBs, Alternative" = "coolers, fmbs, wine, alternative",
                                                              "Craft" = "craft",
                                                              "Hard Seltzer" = "hard seltzer",
                                                              "Import" = "import",
                                                              "Malt Liquor" = "malt liquor",
                                                              "Near Premium" = "near premium",
                                                              "Premium Light" = "premium light",
                                                              "Premium Regular" = "premium regular",
                                                              "Super Premium" = "super premium"
                                               ),
                                               selected = unique(total_data$mc_subsegment_c)
                            ),
                            
                            checkboxGroupInput("packgroup",
                                               label = h4("Select Pack Group"),
                                               choices = list("Singles" = "Singles", 
                                                              "2pks to 4pks" = "Two_to_Four_Packs",
                                                              "6pks to 8pks" = "Six_to_Eight_Packs",
                                                              "9pks to 15pk 12oz " = "Nine_to_FifteenTwelveOz_Packs", 
                                                              "15pk 16oz to 20pks" = "FiftenSixteenOz_to_Twenty_Packs", 
                                                              "24pks +" = "TwentyFour_Plus_Packs"), 
                                               selected = unique(total_data$pack_group))
                          ),
                          # Show a plot of the generated distribution
                          mainPanel(
                            plotOutput("rev_plot"),
                            plotOutput("units_plot")
                            
                          )
                        )
                        ,
                        # valueBoxes
                        fluidRow(
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("sayhomestartTxt"),
                                   tags$head(tags$style("#sayhomestartTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 )),
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("totalRevTxt"),
                                   tags$head(tags$style("#totalRevTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 )),
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("totalUnitsTxt"),
                                   tags$head(tags$style("#totalUnitsTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 ))),
                        fluidRow(
                          column(width = 4
                            ),
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("totalRevActualsTxt"),
                                   tags$head(tags$style("#totalRevActualsTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 )),
                          column(width = 4, 
                                 wellPanel(
                                   textOutput("totalUnitsActualsTxt"),
                                   tags$head(tags$style("#totalUnitsActualsTxt{color: black;
                                 font-size: 20px;
                                 font-style: bold;
                                 }"
                                   )
                                   )
                                 ))),                        
                        fluidRow(
                          column(width = 4,
                                 wellPanel(  radioButtons("rev_or_units_index", label = h4("Select Metric:"),
                                                          choices = list("Revenue vs. Forecasts Index" = "revenue_forecasts_index",
                                                                         "Units vs. Forecasts Index" = "units_forecasts_index",
                                                                         "Revenue vs. Forecasts Actuals" = "revenue_forecasts_actuals",
                                                                         "Units vs. Forecasts Actuals"="units_forecasts_actuals",
                                                                         "Revenue vs. Revenue Year Ago Index" = "revenue_ya_index",
                                                                         "Units vs. Units Year Ago Index"="units_ya_index",
                                                                         "Revenue vs. Revenue Year Ago Actuals" = "revenue_ya_actuals",
                                                                         "Units vs. Units Year Ago Actuals"="units_ya_actuals"), 
                                                          selected = "revenue_forecasts_index")
                                 ) 
                          ),
                          
                          column(width = 8,
                                 dataTableOutput("tbl")  
                          )
                          
                        )
                      )
             )
             ,
             
             tabPanel("Scenario Planner",
                      fluidPage(
                        wellPanel( h3("Forecasting Selections:"),
                        fluidRow(
                          column(width = 2,
                          selectInput("retailer_market2", 
                                      label = h5("Select Retailer Market"),
                                      choices = unique(total_data$market_display_name_mlr)
                                      # selected = unique(data$store_name)
                              )
                            ),
                          column(width= 2,
                                
                                   selectInput("future_trend", 
                                               label = h5("Choose Forecast Trend"),
                                               choices = list("COVID-19 Trend Continues", "Revert to Pre-COVID-19 Trend", "Custom Changes"
                                               )
                                   )# selected = unique(data$store_name)
                                 
                              ),
                          column(width = 2,
                                 
                        selectInput("future_level", 
                                    label = h5("Choose Forecast Level"),
                                    choices = list("Total Geo/Retailer", "Segment", "Segment/Pack"
                                    )
                                )# selected = unique(data$store_name)
                            
                          ),
                        column(width = 2,
                               
                                 radioButtons("forecast_timeframe", label = h5("Forecast Timeframe:"),
                                              choices = list("90 Days",
                                                             "180 Days"), 
                                              selected = "90 Days")
                                
                              ),
                        column(width = 2,
                               
                               radioButtons("forecast_metric", label = h5("Forecast Metric:"),
                                            choices = list("Revenue",
                                                           "Units"), 
                                            selected = "Revenue")
                               
                        )
                          )
                        ),
                        wellPanel( h3("Historical Trends (For Reference):"),
                        fluidRow(
                          column(width = 2,
                                 
                                 radioButtons("metric_select2", label = h5("Choose Metric:"),
                                              choices = list("Expected vs. Actual",
                                                             "Year Over Year"), 
                                              selected = "Expected vs. Actual")
                          ),
                          column(width = 5,
                                 h4("Beginning of year to 3-07-2020:"),
                                 dataTableOutput("tbl_beg_yr_to_march_break_please")  
                                 
                          ),
                          column(width = 5, 
                                 h4("3-07-2020 to Current:"),
                                 
                                 dataTableOutput("tbl_march_to_now")  
                                 
                          )
                         )
                        ),
                        wellPanel( h3("Custom Segment Trend Changes (in %):"),
                        fluidRow(
                          column(width = 2,
                                 numericInput("num", label = h5("Budget"), value = 5
                                 ),
                                 numericInput("num", label = h5("Cider"), value = 5
                                 )),
                          column(width = 2,
                                 numericInput("num", label = h5("Coolers, FMBs, Alternative"), value = 5
                                 ),
                                 numericInput("num", label = h5("Craft"), value = 5
                                 )),
                          column(width = 2,
                                 numericInput("num", label = h5("Hard Seltzer"), value = 5
                                 ),
                                 numericInput("num", label = h5("Import"), value = 5
                                 )),
                          column(width = 2,
                                 numericInput("num", label = h5("Malt Liquor"), value = 5
                                 ),
                                 numericInput("num", label = h5("Near Premium"), value = 5
                                 )
                          ),
                          column(width = 2,
                                 numericInput("num", label = h5("Premium Light"), value = 5
                                 ),
                                 numericInput("num", label = h5("Premium Regular"), value = 5
                                 )),
                          column(width = 2,
                                 numericInput("num", label = h5("Super Premium"), value = 5
                                 ),
                                 numericInput("num", label = h5("Premium Regular"), value = 5
                                 ),
                                 numericInput("num", label = h5("Super Premium"), value = 5
                                 )
                          )
                          )  
                        ),
                        wellPanel(h3("Future Impact:") ,
                        fluidRow(
                          column(width = 2
                          ),
                          column(width = 8,
                                 dataTableOutput("tbl_beg_yr_to_march")  
                                 ),
                          column(width = 2
                          ))
                        )
                      )
             )
  ,
tabPanel("Resources",
         fluidPage(
           fluidRow(
             column(width = 4
             ),
             column(width = 4, 
                    h2("External Resources:")
             ),
             column(width = 4
             )),
           
           fluidRow(
             column(width = 4
             ),
             column(   width = 4, h4(  a("COVID-19 Metrics By State",     href="https://public.tableau.com/profile/peter.james.walker#!/")
             ))
             ,
             column(width = 4
             )
           )
           ,
           
           fluidRow(
             column(width = 4
             ),
             column(   width = 4, h4(  a("Unemployment Claims By State",     href="https://public.tableau.com/profile/akshada.bhanushali#!/vizhome/State-LevelUnemploymentInsuranceClaimsDuringCoronavirusOutbreak/Dashboard1")
               ))
             ,
             column(width = 4
             )
           ),
       fluidRow(
         column(width = 2),
         column(width = 8,
                tags$iframe(
                  seamless = "seamless", 
                  src =  "https://static.usafacts.org/public/2020/coronavirus-timeline/county-embed.html", 
                  height = 700, width = "100%"
                ),
                column(width = 2)
         )
       ),           
       fluidRow(
         column(width = 2),
         column(width = 8,
                tags$iframe(
                  seamless = "seamless", 
                  src =  "https://usafacts.org/articles/covid-19-impact-jobs-health-state-finances/embed/5/?", 
                  height = 1000, width = 750
                ),
                column(width = 2)
         )
       ),           
       fluidRow(
         column(width = 1),
         column(width = 10,
                tags$iframe(
                  seamless = "seamless", 
                  src =  "https://usafacts.org/embed/chart/?adjustment&axisTextColor=%23616161&chartType=0&margins=%5Bobject%20Object%5D&metrics=%5B%7B%22id%22%3A%22sotu2051%22%7D%2C%7B%22id%22%3A%22sotu2044%22%7D%2C%7B%22id%22%3A%22sotu2041%22%7D%2C%7B%22id%22%3A%22sotu2047%22%7D%2C%7B%22id%22%3A%22sotu2048%22%7D%5D&source=SOTU&startYear=2007&title=Unemployment%20rate%20by%20race%2Fethnicity&titleFontSize=14&titleHorizontalAlignment=center&titleVerticalPosition=bottom", 
                  height = 900, width = "100%"
                )
         ),
         column(width = 1)
       )

         )
)
)
))
