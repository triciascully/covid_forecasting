"""
Author:
Tricia Scully
Pricing Strategy & Analytics Manager
4.23.2020

- Code Details - 

Business Purpose: create forecasts for retail/state/segment/pack groups for COVID impact analysis
Data sources: Nielsen in AIP
Region: AZ, FL
Brand: Circle K
Segment: All
Pack Size: All
SKU: All
Timeframe: 2 yrs. through shelter in place dates by state


Notes: n/a
"""
import pandas as pd
import pyodbc
from datetime import datetime, timedelta
import pmdarima as pm
from pmdarima import model_selection
from pandas.plotting import register_matplotlib_converters
import numpy as np

register_matplotlib_converters()

start = datetime.now()

def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    return data

# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()

## Store level data
query = """ select period_description_short,
 store_name,
 state,
 mc_subsegment_c,
 sum(units) as units,
 sum(dollars) as dollars,

CASE
    WHEN mc_package_size_c = '1pk' THEN 'Singles'
    WHEN mc_package_size_c in ('2pk','3pk','4pk','6pk','8pk' ,'9pk', '10pk') THEN 'Two_to_Ten_Packs'
    WHEN mc_package_size_c in ('12pk','15pk','18pk','20pk') THEN 'Twelve_to_Twenty_Packs'
    ELSE 'TwentyFour_to_36_Packs'
END AS pack_group,
CASE
    WHEN mc_subsegment_c = 'wine' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c = 'hard seltzer' THEN 'hard seltzer'
    WHEN mc_subsegment_c = 'alternative' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'coolers' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c IN ('fruit/spice beer','porter/stout/brown','not applicable') THEN 'coolers, fmbs, wine, alternative'
    ELSE mc_subsegment_c
END AS mc_subsegment_c2

from

(select store_name , state_abbreviation as state , sum(units) as units , sum(dollar) as 'dollars' , period_description_short , mc_subsegment_c ,
 mc_package_size_c , mc_package_c, mc_mega_style_c 
 from 
 (select a.store_name, a.state_abbreviation, b.units, b.dollar, b.upc, b.period_description_short,  
 c.mc_subsegment_c, c.mc_package_size_c, c.mc_package_c, c.mc_mega_style_c
 
 from  (select tdlinx_store_code, store_name, state_abbreviation from (select distinct * from core.acn_outlet_dim)
 x where store_name = 'circle k' and state_abbreviation in ('fl','az')) 
 
 a inner join (select distinct * from (select tdoutletcd, period_description_short, units, dollar, upc from core.acn_stc_outlet) y ) b 
 
 on a.tdlinx_store_code = b.tdoutletcd 
 
 left join  (select mc_subsegment_c, upc, mc_package_size_c, mc_package_c, mc_mega_style_c from (select 
 distinct * from core.acn_product_dim) z ) c 
 on b.upc = c.upc  ) t  
 group by  store_name , state_abbreviation , upc , period_description_short , mc_subsegment_c , mc_package_size_c , 
 mc_package_c, mc_mega_style_c
) j

 where mc_package_size_c not in ('keg','rem pack size') 

group by 
period_description_short,
store_name,
state,
mc_subsegment_c,
pack_group,
mc_mega_style_c

  """

#pull in data from Impala, create dfs
modeling_data = impala_select(cnxn,query)

#create month, quarter, season flags
modeling_data['date'] = pd.to_datetime(modeling_data['period_description_short'])
modeling_data['month'] = pd.DatetimeIndex(modeling_data['period_description_short']).month
modeling_data['mc_subsegment_c'] = modeling_data['mc_subsegment_c2']

def func(row):
    if row['month'] in (1,2,3):
        return 'q1'
    elif row['month'] in (4,5,6):
        return 'q2' 
    elif row['month'] in (7,8,9):
        return 'q3'    
    else:
        return 'q4'

modeling_data['quarter'] = modeling_data.apply(func, axis=1)

#add in holiday flags
##first bring in holiday table from AIP
holidays_query = "select * from tara_beals.wk_beer_ind_wk_ending_sat where event_date between '2017-01-01' and '2022-01-01' "
                
holidays = impala_select(cnxn,holidays_query)

holidays['holiday_flag'] = 1

# create a new column for merging, format it as datetime
holidays['date'] = pd.to_datetime(holidays['week_end'])

modeling_data = modeling_data.merge(holidays[['date','holiday_flag']], how='left', left_on='date', right_on='date')

#make sure holiday_flag column as 0's in place of NaNs
modeling_data['holiday_flag'] = modeling_data['holiday_flag'].fillna(0)

#create pre-holiday period flag (two weeks prior to holiday week)
#and post-holiday flag (one week after holiday week)

modeling_data['pre_holiday_wk1'] = modeling_data['date'] - timedelta(days = 7)
modeling_data['pre_holiday_wk2'] = modeling_data['date'] - timedelta(days = 14)
modeling_data['post_holiday_wk1'] = modeling_data['date'] + timedelta(days = 7)

leads_lags = modeling_data[(modeling_data['holiday_flag'] == 1)]
leads_lags = leads_lags[['pre_holiday_wk1','pre_holiday_wk2','post_holiday_wk1']].drop_duplicates()
leads_lags['post_holiday_flag'] = 1
leads_lags['pre_holiday_1wk_flag'] = 1
leads_lags['pre_holiday_2wk_flag'] = 1

modeling_data = modeling_data.merge(leads_lags[['pre_holiday_wk1','pre_holiday_1wk_flag']], how = 'left', on='pre_holiday_wk1')
modeling_data['pre_holiday_1wk_flag'] = modeling_data['pre_holiday_1wk_flag'].fillna(0)

modeling_data = modeling_data.merge(leads_lags[['pre_holiday_wk2','pre_holiday_2wk_flag']], how = 'left', on='pre_holiday_wk2')
modeling_data['pre_holiday_2wk_flag'] = modeling_data['pre_holiday_2wk_flag'].fillna(0)

modeling_data = modeling_data.merge(leads_lags[['post_holiday_wk1','post_holiday_flag']], how = 'left', on='post_holiday_wk1')
modeling_data['post_holiday_flag'] = modeling_data['post_holiday_flag'].fillna(0)

#create grouping variable for easier looping in modeling phase:
modeling_data['store_name2'] =  modeling_data['store_name'].astype(str).str.replace(u'\xa0', '')
modeling_data['store_name2'] =  modeling_data['store_name'].astype(str).str.replace(' ', '_')

modeling_data['mc_subsegment_c2'] =  modeling_data['mc_subsegment_c'].astype(str).str.replace(' ', '_')

modeling_data['model_group'] = modeling_data['store_name2']+'_'+modeling_data['state']+'_'+modeling_data['mc_subsegment_c2']+'_'+modeling_data['pack_group']


##bring in COVID date table to customize the beginning of shelter-in-place mandates by state
#data collected from the Institute for Health Metrics Evaluation https://covid19.healthdata.org/united-states-of-america
covid_dates_query = "select state, stay_home_start_date from patricia_scully.covid_dates_by_state "
                
covid_dates = impala_select(cnxn,covid_dates_query)

covid_dates['stay_home_start_date'] = pd.to_datetime(covid_dates['stay_home_start_date'])

# =============================================================================
# Modeling - ARIMA
# =============================================================================

#predictions = {}

for i in list(modeling_data['model_group'].unique()):

    df = modeling_data[(modeling_data['model_group'] == i)].sort_values(by='date')

    df = df.merge(covid_dates, how= 'left', on= 'state')
    #skip modeling_groups that dropped distribution or sales well before COVID happened so forecasts aren't created for 
    # pre-COVID weeks on those groups
    if max(df['date']) < max(df['stay_home_start_date']):
        df = df[['store_name','state', 'mc_subsegment_c','pack_group']].drop_duplicates()
        for row in df.itertuples(index=True, name='Pandas'):
            print(row)
            cursor.execute("INSERT INTO patricia_scully.covid_model_skips(store_name, state, mc_subsegment_c, pack_group) values(?,?,?,?)", (getattr(row, 'store_name')),(getattr(row, 'state')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
    
    if len(df) < 52:
        df = df[['store_name','state', 'mc_subsegment_c','pack_group']].drop_duplicates()
        
        for row in df.itertuples(index=True, name='Pandas'):
            print(row)
            cursor.execute("INSERT INTO patricia_scully.covid_model_skips(store_name, state, mc_subsegment_c, pack_group) values(?,?,?,?)", (getattr(row, 'store_name')),(getattr(row, 'state')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))

    else:
        df = df[(df['date']< max(df['stay_home_start_date']))]
        
        total_units = df['units'].sum()
        total_revenue = df['dollars'].sum()
        avg_price = total_revenue/total_units
        df = df[['date','store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group', 'units','dollars']]
        store_name = df['store_name'].unique()
        state = df['state'].unique()
        mc_subsegment_c = df['mc_subsegment_c'].unique()
        pack_group = df['pack_group'].unique()
        model_group = df['model_group'].unique()
        df = df.groupby(['date','store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})
        df = df[['date','units']].sort_values(by= 'date')
            
        df = df.sort_values(by='date')
        max_date = max(df['date'])
        min_date = min(df['date'])
        df = df.set_index('date')
    
        #fill in missing dates
        df = df.resample("W").ffill()
    
        #create auto arima model
        fit2 = pm.auto_arima(df, 
                            #m=12,
                             D = 1,
                             error_action='ignore',
                             suppress_warnings=True,
                             stepwise=True,
                             random_state = 11, 
                             maxiter = 5
                             
                           #  start_p=1, start_q=1,
                           #          test='adf',
                           #          max_p=3, max_q=3, m=52,
                           #          start_P=0, seasonal=True,
                           #          d=None, trace=True,
                           #          error_action='ignore', maxiter=5,  
                           #          suppress_warnings=True, 
                           #          stepwise=True
                            )
    
        fit2.fit(df)
        
        cv = model_selection.SlidingWindowForecastCV(window_size=26, step=10, h=1)
        model_cv_scores = model_selection.cross_val_score(
            fit2, df, scoring='smape', cv=cv, verbose=2)
        
        m1_average_error = np.average(model_cv_scores)

        #create blank dataframe of dates from end of current data through the rest of the year
        index = pd.date_range(start= max_date, periods=42, freq='W-SAT')
        df_forecasts = pd.DataFrame(index=index)
    
        #fit2.summary()
        #create predictions for rest of the year 
        forecasts = fit2.predict(n_periods=len(df_forecasts))
    
        df_forecasts['forecasted_units'] = forecasts
        df_forecasts['store_name'] = store_name[0]
        df_forecasts['state'] = state[0]
        df_forecasts['mc_subsegment_c'] = mc_subsegment_c[0]
        df_forecasts['pack_group'] = pack_group[0]
        df_forecasts['model_group'] = model_group[0]
        df_forecasts.index.names = ['date']
        df_forecasts.reset_index(inplace=True)
        df_forecasts['forecasted_revenue'] =  df_forecasts['forecasted_units'].astype(float)*avg_price.astype(float)
    
        df_forecasts = df_forecasts[['date', 'store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units','forecasted_revenue']]
    
        df_forecasts = df_forecasts.rename(columns={'date': 'week_ending_date'})
        df_forecasts['week_ending_date'] = df_forecasts['week_ending_date'].astype(str)
        df_forecasts['forecasted_units'] = round(df_forecasts['forecasted_units'])
        df_forecasts['forecasted_units'] = df_forecasts['forecasted_units'].astype(str)
        df_forecasts['forecasted_revenue'] = round(df_forecasts['forecasted_revenue'])
        df_forecasts['forecasted_revenue'] = df_forecasts['forecasted_revenue'].astype(str)
        df_forecasts['cv_score'] = m1_average_error
        df_forecasts['cv_score'] = round(df_forecasts['cv_score'])
        df_forecasts['cv_score'] = df_forecasts['cv_score'].astype(str)
         
        #insert dataframe into HUE
        for row in df_forecasts.itertuples(index=True, name='Pandas'):
            print(row)
            cursor.execute("INSERT INTO patricia_scully.covid_forecasts(week_ending_date,store_name,state,mc_subsegment_c,pack_group,model_group,forecasted_units,forecasted_revenue,cv_score) values(?,?,?,?,?,?,?,?,?)", (getattr(row, 'week_ending_date')),(getattr(row, 'store_name')), (getattr(row, 'state')),(getattr(row, 'mc_subsegment_c')), (getattr(row, 'pack_group')), (getattr(row, 'model_group')), (getattr(row, 'forecasted_units')), (getattr(row, 'forecasted_revenue')), (getattr(row, 'cv_score')))

modeling_data['forecasted_units'] = ''
modeling_data['forecasted_revenue'] = ''
modeling_data['forecasted_units'] = modeling_data['forecasted_units'].astype(str)
modeling_data['forecasted_revenue'] = modeling_data['forecasted_revenue'].astype(str)

modeling_data_with_covid_start_date = modeling_data.merge(covid_dates, on = 'state')

historicals_dict = {}

for i in list(modeling_data_with_covid_start_date['state'].unique()):
    df = modeling_data_with_covid_start_date[(modeling_data_with_covid_start_date['state'] == i)]
    stay_home_start = max(df['stay_home_start_date'])
    df = df[(df['date'] <= stay_home_start)]
    historicals_dict[i] = df

historicals=pd.concat(historicals_dict)

historicals = historicals.reset_index(drop = True)

historicals = historicals[[ 'date', 'store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units', 'forecasted_revenue', 'units', 'dollars']]
historicals = historicals.rename(columns={'date': 'week_ending_date'})

modeling_data_str = historicals.astype(str) 
values=("""('"""+modeling_data_str[modeling_data_str.columns[0]]+"""','"""+modeling_data_str[modeling_data_str.columns[1]]+"""','"""+modeling_data_str[modeling_data_str.columns[2]]+"""','""" +modeling_data_str[modeling_data_str.columns[3]]+ """','"""+
        modeling_data_str[modeling_data_str.columns[4]]+"""','"""+modeling_data_str[modeling_data_str.columns[5]]+"""','"""+modeling_data_str[modeling_data_str.columns[6]]+"""','"""+modeling_data_str[modeling_data_str.columns[7]]+"""','"""+modeling_data_str[modeling_data_str.columns[8]]+"""','"""+modeling_data_str[modeling_data_str.columns[9]]+"""')""").str.cat(sep=',')

cnxn.cursor().execute("""insert into table patricia_scully.covid_historicals values """ + values)
cnxn.commit()
