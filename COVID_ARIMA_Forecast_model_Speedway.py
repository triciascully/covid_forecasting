"""
Author:
Tricia Scully
Pricing Strategy & Analytics Manager
5.11.2020

- Code Details - 

Business Purpose: create forecasts for retail/state/segment/pack groups for COVID impact analysis for Speedway data
Data sources: Speedway
Region: All
Brand: Speedway
Segment: All
Pack Size: All
SKU: All
Timeframe: 2 yrs. through shelter in place dates by state


Notes: n/a
"""
import pandas as pd
import pyodbc
from datetime import datetime, timedelta
import pmdarima as pm
from pmdarima import model_selection
from pandas.plotting import register_matplotlib_converters
import numpy as np

register_matplotlib_converters()

start = datetime.now()

def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    return data

# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()

## Store level data
#bring in lines and ladders master reference sheet
modeling_data = pd.read_excel(r"C:\Users\UR69\OneDrive - Molson Coors Brewing Company\Desktop\Speedway By State By SKU By Week thru WE 4-11-20 v2.xlsx")

modeling_data['Time'] =  modeling_data['Time'].astype(str).str.replace('Week Ending ', '')
modeling_data['date'] = pd.to_datetime(modeling_data['Time'])


modeling_data_dict = {}

for i in list(modeling_data['Sub-Segment'].unique()):
    df = modeling_data[(modeling_data['Sub-Segment'] == i)]
    if i == 'CRAFT':
        df['mc_subsegment_c'] =  'craft'
    elif i == 'IMPORT':
        df['mc_subsegment_c'] =  'import'
    elif i == 'CIDER':
        df['mc_subsegment_c'] =  'cider'    
    elif i == 'FMB':
        df['mc_subsegment_c'] =  'coolers, fmbs, wine, alternative'
    elif i == 'COOLER':
        df['mc_subsegment_c'] =  'coolers, fmbs, wine, alternative'
    elif i == 'PREMIUM REGULAR':
        df['mc_subsegment_c'] =  'premium regular'
    elif i == 'SUPER PREMIUM':
        df['mc_subsegment_c'] =  'super premium'
    elif i == 'NEAR PREMIUM':
        df['mc_subsegment_c'] =  'near premium'
    elif i == 'PREMIUM LIGHT':
        df['mc_subsegment_c'] =  'premium light'
    elif i == 'MALT':
        df['mc_subsegment_c'] =  'malt liquor'
    else:
        df['mc_subsegment_c'] =  'budget'
       
    modeling_data_dict[i] = df

modeling_data=pd.concat(modeling_data_dict)

modeling_data = modeling_data.reset_index(drop = True)

#create grouping variable for easier looping in modeling phase:
modeling_data['store_name2'] =  'speedway'
modeling_data['store_name'] =  'speedway'
modeling_data['mc_subsegment_c2'] =  modeling_data['mc_subsegment_c'].astype(str).str.replace(' ', '_')
modeling_data['state'] = modeling_data['State'].str.lower()

modeling_data_dict = {}

#create pack_group variable
for i in list(modeling_data['Package'].unique()):
    df = modeling_data[(modeling_data['Package'] == i)]
    if i in ['6 PK CAN','6 PK BTL','4 PK CAN','4 PK BTL','6/16 OZ ALUM','8 PK CAN','3 PK CAN','20 PK BTL','8/16 OZ ALUM','8/16 ALUM','9/16 OZ ALUM']:
        df['pack_group'] = 'Two_to_Ten_Packs'
    elif i in ['12 PK BTL','15 PK CAN','12 PK CAN','18 PK CAN','18 PK BTL','15/16 OZ ALUM','20 PK CAN','20/16 OZ ALUM','12/16 OZ ALUM']:
        df['pack_group'] = 'Twelve_to_Twenty_Packs'
    elif i in ['28 PK CAN','30 PK CAN','24 PK BTL','24 PK CAN','36 PK CAN','24/16 OZ ALUM']:
        df['pack_group'] = 'TwentyFour_to_36_Packs'
    else:
        df['pack_group'] = 'Singles'

    modeling_data_dict[i] = df

modeling_data=pd.concat(modeling_data_dict)

modeling_data = modeling_data.reset_index(drop = True)

modeling_data['model_group'] = modeling_data['store_name2']+'_'+modeling_data['state']+'_'+modeling_data['mc_subsegment_c2']+'_'+modeling_data['pack_group']

modeling_data['units'] = modeling_data['Unit Sales']
modeling_data['dollars'] = modeling_data['Dollar Sales']


##bring in COVID date table to customize the beginning of shelter-in-place mandates by state
#data collected from the Institute for Health Metrics Evaluation https://covid19.healthdata.org/united-states-of-america
covid_dates_query = "select state, stay_home_start_date from patricia_scully.covid_dates_by_state "
                
covid_dates = impala_select(cnxn,covid_dates_query)

#Hard-code COVID start-date for the time being until IHME data is more complete
covid_dates['stay_home_start_date'] = '2020-03-01'

covid_dates['stay_home_start_date'] = pd.to_datetime(covid_dates['stay_home_start_date'])

# =============================================================================
# Modeling - ARIMA
# =============================================================================

#predictions = {}

for i in list(modeling_data['model_group'].unique()):

    df = modeling_data[(modeling_data['model_group'] == i)].sort_values(by='date')

    df = df.merge(covid_dates, how= 'left', on= 'state')
    #skip modeling_groups that dropped distribution or sales well before COVID happened so forecasts aren't created for 
    # pre-COVID weeks on those groups
    if max(df['date']) < max(df['stay_home_start_date']):
        df = df[['store_name','state', 'mc_subsegment_c','pack_group']].drop_duplicates()
        for row in df.itertuples(index=True, name='Pandas'):
            print(row)
            cursor.execute("INSERT INTO patricia_scully.covid_model_skips(store_name, state, mc_subsegment_c, pack_group) values(?,?,?,?)", (getattr(row, 'store_name')),(getattr(row, 'state')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
    
    if len(df) < 52:
        df = df[['store_name','state', 'mc_subsegment_c','pack_group']].drop_duplicates()
        
        for row in df.itertuples(index=True, name='Pandas'):
            print(row)
            cursor.execute("INSERT INTO patricia_scully.covid_model_skips(store_name, state, mc_subsegment_c, pack_group) values(?,?,?,?)", (getattr(row, 'store_name')),(getattr(row, 'state')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))

    else:
        df = df[(df['date']< max(df['stay_home_start_date']))]
        
        total_units = df['units'].sum()
        total_revenue = df['dollars'].sum()
        avg_price = total_revenue/total_units
        df = df[['date','store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group', 'units','dollars']]
        store_name = df['store_name'].unique()
        state = df['state'].unique()
        mc_subsegment_c = df['mc_subsegment_c'].unique()
        pack_group = df['pack_group'].unique()
        model_group = df['model_group'].unique()
        df = df.groupby(['date','store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})
        df = df[['date','units']].sort_values(by= 'date')
            
        df = df.sort_values(by='date')
        max_date = max(df['date'])
        min_date = min(df['date'])
        df = df.set_index('date')
    
        #fill in missing dates
        df = df.resample("W").ffill()
    
        #create auto arima model
        fit2 = pm.auto_arima(df, 
                            #m=12,
                             D = 1,
                             error_action='ignore',
                             suppress_warnings=True,
                             stepwise=True,
                             random_state = 11, 
                             maxiter = 5
                             
                           #  start_p=1, start_q=1,
                           #          test='adf',
                           #          max_p=3, max_q=3, m=52,
                           #          start_P=0, seasonal=True,
                           #          d=None, trace=True,
                           #          error_action='ignore', maxiter=5,  
                           #          suppress_warnings=True, 
                           #          stepwise=True
                            )
    
        fit2.fit(df)
        
        cv = model_selection.SlidingWindowForecastCV(window_size=26, step=10, h=1)
        model_cv_scores = model_selection.cross_val_score(
            fit2, df, scoring='smape', cv=cv, verbose=2)
        
        m1_average_error = np.average(model_cv_scores)

        #create blank dataframe of dates from end of current data through the rest of the year
        index = pd.date_range(start= max_date, periods=42, freq='W-SAT')
        df_forecasts = pd.DataFrame(index=index)
    
        #fit2.summary()
        #create predictions for rest of the year 
        forecasts = fit2.predict(n_periods=len(df_forecasts))
    
        df_forecasts['forecasted_units'] = forecasts
        df_forecasts['store_name'] = store_name[0]
        df_forecasts['state'] = state[0]
        df_forecasts['mc_subsegment_c'] = mc_subsegment_c[0]
        df_forecasts['pack_group'] = pack_group[0]
        df_forecasts['model_group'] = model_group[0]
        df_forecasts.index.names = ['date']
        df_forecasts.reset_index(inplace=True)
        df_forecasts['forecasted_revenue'] =  df_forecasts['forecasted_units'].astype(float)*avg_price.astype(float)
    
        df_forecasts = df_forecasts[['date', 'store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units','forecasted_revenue']]
    
        df_forecasts = df_forecasts.rename(columns={'date': 'week_ending_date'})
        df_forecasts['week_ending_date'] = df_forecasts['week_ending_date'].astype(str)
        df_forecasts['forecasted_units'] = round(df_forecasts['forecasted_units'])
        df_forecasts['forecasted_units'] = df_forecasts['forecasted_units'].astype(str)
        df_forecasts['forecasted_revenue'] = round(df_forecasts['forecasted_revenue'])
        df_forecasts['forecasted_revenue'] = df_forecasts['forecasted_revenue'].astype(str)
        df_forecasts['cv_score'] = m1_average_error
        df_forecasts['cv_score'] = round(df_forecasts['cv_score'])
        df_forecasts['cv_score'] = df_forecasts['cv_score'].astype(str)
         
        #insert dataframe into HUE
        for row in df_forecasts.itertuples(index=True, name='Pandas'):
            print(row)
            cursor.execute("INSERT INTO patricia_scully.covid_forecasts(week_ending_date,store_name,state,mc_subsegment_c,pack_group,model_group,forecasted_units,forecasted_revenue,cv_score) values(?,?,?,?,?,?,?,?,?)", (getattr(row, 'week_ending_date')),(getattr(row, 'store_name')), (getattr(row, 'state')),(getattr(row, 'mc_subsegment_c')), (getattr(row, 'pack_group')), (getattr(row, 'model_group')), (getattr(row, 'forecasted_units')), (getattr(row, 'forecasted_revenue')), (getattr(row, 'cv_score')))

modeling_data['forecasted_units'] = ''
modeling_data['forecasted_revenue'] = ''
modeling_data['forecasted_units'] = modeling_data['forecasted_units'].astype(str)
modeling_data['forecasted_revenue'] = modeling_data['forecasted_revenue'].astype(str)

modeling_data_with_covid_start_date = modeling_data.merge(covid_dates, on = 'state')

historicals_dict = {}

for i in list(modeling_data_with_covid_start_date['state'].unique()):
    df = modeling_data_with_covid_start_date[(modeling_data_with_covid_start_date['state'] == i)]
    stay_home_start = max(df['stay_home_start_date'])
    df = df[(df['date'] <= stay_home_start)]
    historicals_dict[i] = df

historicals=pd.concat(historicals_dict)

historicals = historicals.reset_index(drop = True)

historicals = historicals[[ 'date', 'store_name', 'state', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units', 'forecasted_revenue', 'units', 'dollars']]
historicals = historicals.rename(columns={'date': 'week_ending_date'})

modeling_data_str = historicals.astype(str) 
values=("""('"""+modeling_data_str[modeling_data_str.columns[0]]+"""','"""+modeling_data_str[modeling_data_str.columns[1]]+"""','"""+modeling_data_str[modeling_data_str.columns[2]]+"""','""" +modeling_data_str[modeling_data_str.columns[3]]+ """','"""+
        modeling_data_str[modeling_data_str.columns[4]]+"""','"""+modeling_data_str[modeling_data_str.columns[5]]+"""','"""+modeling_data_str[modeling_data_str.columns[6]]+"""','"""+modeling_data_str[modeling_data_str.columns[7]]+"""','"""+modeling_data_str[modeling_data_str.columns[8]]+"""','"""+modeling_data_str[modeling_data_str.columns[9]]+"""')""").str.cat(sep=',')

cnxn.cursor().execute("""insert into table patricia_scully.covid_historicals values """ + values)
cnxn.commit()
