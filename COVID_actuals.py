"""
Author:
Tricia Scully
Pricing Strategy & Analytics Manager
5.4.2020

- Code Details - 

Business Purpose: merge actual unit and dollars records onto forecasts created in COVID modeling phase
Data sources: Nielsen in AIP
Region: States
Brand: Circle K
Segment: All
Pack Size: All
SKU: All
Timeframe: 2 yrs. through 3.14.2020


Notes: n/a
"""


#Import all required packages
import pandas as pd
import pyodbc
from datetime import datetime
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

start = datetime.now()

def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    return data

# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()


######################################################################################
#
#  STC Actuals Pull
#
######################################################################################


#get COVID records to add actuals onto forecasted 
actuals_query = """ select period_description_short,
 store_name,
 state,
 mc_subsegment_c,
 sum(units) as units,
 sum(dollars) as dollars,

CASE
    WHEN mc_package_size_c = '1pk' THEN 'Singles'
    WHEN mc_package_size_c in ('2pk','3pk','4pk','6pk','8pk' ,'9pk', '10pk') THEN 'Two_to_Ten_Packs'
    WHEN mc_package_size_c in ('12pk','15pk','18pk','20pk') THEN 'Twelve_to_Twenty_Packs'
    ELSE 'TwentyFour_to_36_Packs'
END AS pack_group,
CASE
    WHEN mc_subsegment_c = 'wine' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c = 'hard seltzer' THEN 'hard seltzer'
    WHEN mc_subsegment_c = 'alternative' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'coolers' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c IN ('fruit/spice beer','porter/stout/brown','not applicable') THEN 'coolers, fmbs, wine, alternative'
    ELSE mc_subsegment_c
END AS mc_subsegment_c2

from

(select store_name , state_abbreviation as state , sum(units) as units , sum(dollar) as 'dollars' , period_description_short , mc_subsegment_c ,
 mc_package_size_c , mc_package_c, mc_mega_style_c 
 from 
 (select a.store_name, a.state_abbreviation, b.units, b.dollar, b.upc, b.period_description_short,  
 c.mc_subsegment_c, c.mc_package_size_c, c.mc_package_c, c.mc_mega_style_c
 
 from  (select tdlinx_store_code, store_name, state_abbreviation from (select distinct * from core.acn_outlet_dim)
 x where store_name = 'circle k' and state_abbreviation in ('fl','az')) 
 
 a inner join (select distinct * from (select tdoutletcd, period_description_short, units, dollar, upc from core.acn_stc_outlet) y ) b 
 
 on a.tdlinx_store_code = b.tdoutletcd 
 
 left join  (select mc_subsegment_c, upc, mc_package_size_c, mc_package_c, mc_mega_style_c from (select 
 distinct * from core.acn_product_dim) z ) c 
 on b.upc = c.upc  ) t  
 group by  store_name , state_abbreviation , upc , period_description_short , mc_subsegment_c , mc_package_size_c , 
 mc_package_c, mc_mega_style_c
) j

 where mc_package_size_c not in ('keg','rem pack size') 

group by 
period_description_short,
store_name,
state,
mc_subsegment_c,
pack_group,
mc_mega_style_c
  """

#pull in data from Impala, create dfs
actuals_data = impala_select(cnxn,actuals_query)

actuals_data['date'] = pd.to_datetime(actuals_data['period_description_short'])
actuals_data['mc_subsegment_c'] = actuals_data['mc_subsegment_c2']

##bring in COVID date table to customize the beginning of shelter-in-place mandates by state
#data collected from the Institute for Health Metrics Evaluation https://covid19.healthdata.org/united-states-of-america
covid_dates_query = "select state, stay_home_start_date from patricia_scully.covid_dates_by_state "
                
covid_dates = impala_select(cnxn,covid_dates_query)

#Hard-code COVID start-date for the time being until IHME data is more complete
covid_dates['stay_home_start_date'] = '2020-03-01'

covid_dates['stay_home_start_date'] = pd.to_datetime(covid_dates['stay_home_start_date'])

actuals_data = actuals_data.merge(covid_dates, how= 'left', on= 'state')

actuals_data = actuals_data.copy()

#create grouping variable:
actuals_data['store_name2'] =  actuals_data['store_name'].astype(str).str.replace(u'\xa0', '')
actuals_data['store_name2'] =  actuals_data['store_name'].astype(str).str.replace(' ', '_')

actuals_data['mc_subsegment_c2'] =  actuals_data['mc_subsegment_c'].astype(str).str.replace(' ', '_')

actuals_data['model_group'] = actuals_data['store_name2']+'_'+actuals_data['state']+'_'+actuals_data['mc_subsegment_c2']+'_'+actuals_data['pack_group']
actuals_data['week_ending_date'] = actuals_data['date']
actuals_data = actuals_data[['week_ending_date', 'model_group', 'units', 'dollars','stay_home_start_date']]
actuals_data = actuals_data.groupby(['week_ending_date','model_group','stay_home_start_date'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})

actuals_dict = {}

for i in list(actuals_data['model_group'].unique()):
    df = actuals_data[(actuals_data['model_group'] == i)]
    stay_home_start = max(df['stay_home_start_date'])
    df = df[(df['week_ending_date'] >= stay_home_start)]
    actuals_dict[i] = df

actuals=pd.concat(actuals_dict)

actuals_data = actuals.reset_index(drop = True)

actuals_data = actuals_data[['week_ending_date', 'model_group', 'units', 'dollars']]

actuals_data['week_ending_date'] = actuals_data['week_ending_date'].astype(str)
actuals_data['units'] = actuals_data['units'].astype(str)
actuals_data['dollars'] = actuals_data['dollars'].astype(str)

#delete table if table exists
cur = cnxn.cursor()
string = "drop table if exists patricia_scully.covid_actuals"
cur.execute(string)
cnxn.commit()

#create new table
string ="""create table patricia_scully.covid_actuals (week_ending_date STRING, model_group STRING, units STRING, dollars STRING); """
cur.execute(string)
cnxn.commit()

##insert actuals into table
#for row in actuals_data.itertuples(index=True, name='Pandas'):
#    print(row)
#    cursor.execute("INSERT INTO patricia_scully.covid_actuals(week_ending_date,model_group,units, dollars) values(?,?,?,?)", (getattr(row, 'week_ending_date')), (getattr(row, 'model_group')), (getattr(row, 'units')), (getattr(row, 'dollars')))
#    

actuals_data_str = actuals_data.astype(str) 
values=("""('"""+actuals_data_str[actuals_data_str.columns[0]]+"""','"""+actuals_data_str[actuals_data_str.columns[1]]+"""','"""+actuals_data_str[actuals_data_str.columns[2]]+"""','""" +actuals_data_str[actuals_data_str.columns[3]]+ """')""").str.cat(sep=',')


cnxn.cursor().execute("""insert into table patricia_scully.covid_actuals values """ + values)
cnxn.commit()


######################################################################################
#
#  Speedway Actuals Pull
#
######################################################################################

#get COVID records to add actuals onto forecasted 
actuals_query = """ select * from patricia_scully.speedway_thru_july
  """

#pull in data from Impala, create dfs
actuals_data = impala_select(cnxn,actuals_query)

actuals_data['time'] =  actuals_data['time'].astype(str).str.replace('Week Ending ', '')
actuals_data['date'] = pd.to_datetime(actuals_data['time'])
#754539
actuals_data = actuals_data[(actuals_data['date'] >= '2020-03-01')]
#40674
actuals_data_dict = {}

for i in list(actuals_data['sub_segment'].unique()):
    df = actuals_data[(actuals_data['sub_segment'] == i)]
    if i == 'CRAFT':
        df['mc_subsegment_c'] =  'craft'
    elif i == 'IMPORT':
        df['mc_subsegment_c'] =  'import'
    elif i == 'CIDER':
        df['mc_subsegment_c'] =  'cider'    
    elif i == 'FMB':
        df['mc_subsegment_c'] =  'coolers, fmbs, wine, alternative'
    elif i == 'COOLER':
        df['mc_subsegment_c'] =  'coolers, fmbs, wine, alternative'
    elif i == 'PREMIUM REGULAR':
        df['mc_subsegment_c'] =  'premium regular'
    elif i == 'SUPER PREMIUM':
        df['mc_subsegment_c'] =  'super premium'
    elif i == 'NEAR PREMIUM':
        df['mc_subsegment_c'] =  'near premium'
    elif i == 'PREMIUM LIGHT':
        df['mc_subsegment_c'] =  'premium light'
    elif i == 'MALT':
        df['mc_subsegment_c'] =  'malt liquor'
    else:
        df['mc_subsegment_c'] =  'budget'
       
    actuals_data_dict[i] = df

actuals_data=pd.concat(actuals_data_dict)

actuals_data = actuals_data.reset_index(drop = True)

#create grouping variable for easier looping in modeling phase:
actuals_data['store_name2'] =  'speedway'
actuals_data['store_name'] =  'speedway'
actuals_data['mc_subsegment_c2'] =  actuals_data['mc_subsegment_c'].astype(str).str.replace(' ', '_')
actuals_data['state'] = actuals_data['state'].str.lower()

actuals_data_dict = {}

#create pack_group variable
for i in list(actuals_data['package'].unique()):
    df = actuals_data[(actuals_data['package'] == i)]
    if i in ['4 PK CAN','4 PK BTL','3 PK CAN']:
        df['pack_group'] = 'Two_to_Four_Packs'
    elif i in ['6 PK CAN','6 PK BTL','6/16 OZ ALUM','8 PK CAN','8/16 OZ ALUM','8/16 ALUM']:
        df['pack_group'] = 'Six_to_Eight_Packs'  
    elif i in ['9/16 OZ ALUM','12 PK BTL','15 PK CAN','12 PK CAN','12/16 OZ ALUM']:
        df['pack_group'] = 'Nine_to_FifteenTwelveOz_Packs' 
    elif i in ['20 PK BTL','18 PK CAN','18 PK BTL','15/16 OZ ALUM','20 PK CAN','20/16 OZ ALUM']:
        df['pack_group'] = 'FiftenSixteenOz_to_Twenty_Packs'
    elif i in ['28 PK CAN','30 PK CAN','24 PK BTL','24 PK CAN','36 PK CAN','24/16 OZ ALUM']:
        df['pack_group'] = 'TwentyFour_Plus_Packs'
    else:
        df['pack_group'] = 'Singles'

    actuals_data_dict[i] = df

actuals_data=pd.concat(actuals_data_dict)

actuals_data = actuals_data.reset_index(drop = True)

actuals_data['model_group'] = actuals_data['store_name2']+'_'+actuals_data['state']+'_'+actuals_data['mc_subsegment_c2']+'_'+actuals_data['pack_group']

actuals_data['units'] = actuals_data['unit_sales']
actuals_data['dollars'] = actuals_data['dollar_sales']

actuals_data = actuals_data.copy()

actuals_data['units'] = actuals_data['units'].astype(float)
actuals_data['dollars'] = actuals_data['dollars'].astype(str).str.replace('$', '')
actuals_data['dollars'] =  actuals_data['dollars'].astype(str).str.replace('"', '')
actuals_data['dollars'] =  actuals_data['dollars'].astype(str).str.replace('(', '-')
actuals_data['dollars'] =  actuals_data['dollars'].astype(str).str.replace(')', '')
actuals_data['dollars'] =  actuals_data['dollars'].astype(str).str.replace(' -   ', '')
actuals_data['dollars']  = actuals_data['dollars'].str.strip()

actuals_data = actuals_data[(actuals_data['dollars'] != '')]
actuals_data['dollars'] = actuals_data['dollars'].astype(float)


##bring in COVID date table to customize the beginning of shelter-in-place mandates by state
#data collected from the Institute for Health Metrics Evaluation https://covid19.healthdata.org/united-states-of-america
#covid_dates_query = "select state, stay_home_start_date from patricia_scully.covid_dates_by_state "
                
#covid_dates = impala_select(cnxn,covid_dates_query)

#Hard-code COVID start-date for the time being until IHME data is more complete
#covid_dates['stay_home_start_date'] = '2020-03-01'
#covid_dates['stay_home_start_date'] = pd.to_datetime(covid_dates['stay_home_start_date'])
#actuals_data = actuals_data.merge(covid_dates, how= 'left', on= 'state')
#actuals_data = actuals_data.copy()

actuals_data['stay_home_start_date'] = '2020-03-01'
actuals_data['stay_home_start_date'] = pd.to_datetime(actuals_data['stay_home_start_date'])

actuals_data['week_ending_date'] = actuals_data['date']
actuals_data = actuals_data[['week_ending_date', 'model_group', 'units', 'dollars','stay_home_start_date']]
actuals_data = actuals_data.groupby(['week_ending_date','model_group','stay_home_start_date'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})

actuals_dict = {}

for i in list(actuals_data['model_group'].unique()):
    df = actuals_data[(actuals_data['model_group'] == i)]
    stay_home_start = max(df['stay_home_start_date'])
    df = df[(df['week_ending_date'] >= stay_home_start)]
    actuals_dict[i] = df

actuals=pd.concat(actuals_dict)

actuals_data = actuals.reset_index(drop = True)

actuals_data = actuals_data[['week_ending_date', 'model_group', 'units', 'dollars']]

actuals_data['week_ending_date'] = actuals_data['week_ending_date'].astype(str)
actuals_data['units'] = actuals_data['units'].astype(str)
actuals_data['dollars'] = actuals_data['dollars'].astype(str)


#delete table if table exists
cur = cnxn.cursor()
string = "drop table if exists covid_analysis.covid_actuals_speedway2"
cur.execute(string)
cnxn.commit()

#create new table
string ="""create table covid_analysis.covid_actuals_speedway2 (week_ending_date STRING, model_group STRING, units STRING, dollars STRING); """
cur.execute(string)
cnxn.commit()


##insert actuals into table
#for row in actuals_data.itertuples(index=True, name='Pandas'):
#    print(row)
#    cursor.execute("INSERT INTO patricia_scully.covid_actuals(week_ending_date,model_group,units, dollars) values(?,?,?,?)", (getattr(row, 'week_ending_date')), (getattr(row, 'model_group')), (getattr(row, 'units')), (getattr(row, 'dollars')))
#    

actuals_data_str = actuals_data.astype(str) 
values=("""('"""+actuals_data_str[actuals_data_str.columns[0]]+"""','"""+actuals_data_str[actuals_data_str.columns[1]]+"""','"""+actuals_data_str[actuals_data_str.columns[2]]+"""','""" +actuals_data_str[actuals_data_str.columns[3]]+ """')""").str.cat(sep=',')

cnxn.cursor().execute("""insert into table covid_analysis.covid_actuals_speedway2 values """ + values)
cnxn.commit()






################# tests

#get COVID records to add actuals onto forecasted 
test_query = """ select * from covid_analysis.covid_actuals_speedway3
  """

#pull in data from Impala, create dfs
test_data = impala_select(cnxn,test_query)

test_data_totals = actuals_data.groupby(['week_ending_date'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})


import matplotlib.pyplot as plt

plt.plot(test_data_totals['week_ending_date'],test_data_totals['dollars'])














######################################################################################
#
#  Alberstons Actuals Pull
#
######################################################################################


#get COVID records to add actuals onto forecasted 
actuals_query = """ select period_description_short,
 market_display_name_mlr,
 mc_subsegment_c,
 sum(units) as units,
 sum(dollar) as dollars,

CASE
    WHEN mc_package_size_c = '1pk' THEN 'Singles'
    WHEN mc_package_size_c in ('2pk','3pk','4pk') THEN 'Two_to_Four_Packs'
    WHEN mc_package_size_c in ('6pk','8pk') THEN 'Six_to_Eight_Packs'
    WHEN mc_package_size_c in ('9pk', '10pk','12pk') THEN 'Nine_to_FifteenTwelveOz_Packs'
    WHEN mc_package_size_c in ('15pk') and mc_ounce_c in ('12oz') THEN 'Nine_to_FifteenTwelveOz_Packs'
    WHEN mc_package_size_c in ('15pk') and mc_ounce_c in ('16oz','19.2oz','22oz','25oz','24oz','18oz') THEN 'FiftenSixteenOz_to_Twenty_Packs'
    WHEN mc_package_size_c in ('18pk','20pk') THEN 'FiftenSixteenOz_to_Twenty_Packs'
    ELSE 'TwentyFour_Plus_Packs'
END AS pack_group,
CASE
    WHEN mc_subsegment_c = 'wine' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c = 'hard seltzer' THEN 'hard seltzer'
    WHEN mc_subsegment_c = 'alternative' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'coolers' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c IN ('fruit/spice beer','porter/stout/brown','not applicable') THEN 'coolers, fmbs, wine, alternative'
    ELSE mc_subsegment_c
END AS mc_subsegment_c2

from

(select z.market_display_name_mlr, z.units, z.dollar, z.period_description_short , c.mc_subsegment_c , c.mc_ounce_c, c.mc_package_size_c , c.mc_mega_style_c 
 from 
 (select market_display_name_mlr, units, dollar, upc, period_description_short

 from (select distinct * from core.acn_stc_market) a
 where market_display_name_mlr = 'ALBSCO Dal & Ft Wth TA') z 
 
 left join  (select mc_subsegment_c, upc,mc_ounce_c, mc_package_size_c, mc_mega_style_c from (select 
 distinct * from core.acn_product_dim) f ) c 
 on z.upc = c.upc   
 where mc_package_size_c not in ('keg','rem pack size') ) t
 group by market_display_name_mlr , period_description_short , mc_subsegment_c , mc_package_size_c, mc_subsegment_c2, pack_group

  """

#pull in data from Impala, create dfs
actuals_data = impala_select(cnxn,actuals_query)

actuals_data['date'] = pd.to_datetime(actuals_data['period_description_short'])
actuals_data['mc_subsegment_c'] = actuals_data['mc_subsegment_c2']

#Hard-code COVID start-date
actuals_data['stay_home_start_date'] = '2020-03-01'

actuals_data['stay_home_start_date'] = pd.to_datetime(actuals_data['stay_home_start_date'])

#create grouping variable:
#create grouping variable for easier looping in modeling phase:
actuals_data['store_name2'] =  actuals_data['market_display_name_mlr'].astype(str).str.replace(u'\xa0', '')
actuals_data['store_name2'] =  actuals_data['store_name2'].astype(str).str.replace(' ', '_')

actuals_data['mc_subsegment_c2'] =  actuals_data['mc_subsegment_c'].astype(str).str.replace(' ', '_')

actuals_data['model_group'] = actuals_data['store_name2']+'_'+actuals_data['mc_subsegment_c2']+'_'+actuals_data['pack_group']

actuals_data['week_ending_date'] = actuals_data['date']
actuals_data = actuals_data[['week_ending_date', 'model_group', 'units', 'dollars','stay_home_start_date']]
actuals_data = actuals_data.groupby(['week_ending_date','model_group','stay_home_start_date'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})

actuals_dict = {}

for i in list(actuals_data['model_group'].unique()):
    df = actuals_data[(actuals_data['model_group'] == i)]
    stay_home_start = max(df['stay_home_start_date'])
    df = df[(df['week_ending_date'] >= stay_home_start)]
    actuals_dict[i] = df

actuals=pd.concat(actuals_dict)

actuals_data = actuals.reset_index(drop = True)

actuals_data = actuals_data[['week_ending_date', 'model_group', 'units', 'dollars']]

actuals_data['week_ending_date'] = actuals_data['week_ending_date'].astype(str)
actuals_data['units'] = actuals_data['units'].astype(str)
actuals_data['dollars'] = actuals_data['dollars'].astype(str)

#delete table if table exists
cur = cnxn.cursor()
string = "drop table if exists patricia_scully.covid_actuals_albertsons"
cur.execute(string)
cnxn.commit()

#create new table
string ="""create table patricia_scully.covid_actuals_albertsons (week_ending_date STRING, model_group STRING, units STRING, dollars STRING); """
cur.execute(string)
cnxn.commit()

##insert actuals into table
#for row in actuals_data.itertuples(index=True, name='Pandas'):
#    print(row)
#    cursor.execute("INSERT INTO patricia_scully.covid_actuals(week_ending_date,model_group,units, dollars) values(?,?,?,?)", (getattr(row, 'week_ending_date')), (getattr(row, 'model_group')), (getattr(row, 'units')), (getattr(row, 'dollars')))
#    

actuals_data_str = actuals_data.astype(str) 
values=("""('"""+actuals_data_str[actuals_data_str.columns[0]]+"""','"""+actuals_data_str[actuals_data_str.columns[1]]+"""','"""+actuals_data_str[actuals_data_str.columns[2]]+"""','""" +actuals_data_str[actuals_data_str.columns[3]]+ """')""").str.cat(sep=',')


cnxn.cursor().execute("""insert into table patricia_scully.covid_actuals_albertsons values """ + values)
cnxn.commit()
