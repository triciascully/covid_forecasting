###serial test


#Import all required packages
#from fastdtw import fastdtw
from getpass import getuser as get_user
from getpass import getpass as get_password
#from os import environ as enviroment_vars
from os import setpgrp as set_process_group
#from random import random
from shlex import split as cmd_split
from subprocess import Popen, PIPE, STDOUT

import pandas as pd
import pyodbc
from datetime import datetime
import pmdarima as pm
from pandas.plotting import register_matplotlib_converters
from pmdarima import model_selection
import numpy as np

from scipy.stats import boxcox
from scipy.special import inv_boxcox

import multiprocessing as mp

from joblib import delayed
import time

from time import time # to time the program
from time import sleep # to mimic a slow function
from dask import delayed # to allow parallel computation
import dask

import concurrent.futures
register_matplotlib_converters()

start = datetime.now()

def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    return data

def authenticate_kerberos(force=False):
    # get username
    username = get_user()
    # get kerberos realm
    krb_realm = "MILLER.LOCAL"
    
    # create 'klist" command
    cmd = "klist"
    # create linux command process
    process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
    # wait until done, get result
    result = process.communicate(None)[0].decode("utf-8") 
    
    # check that we don't already have a valid ticket
    found_username = False
    expire_date = None
    for line in result.split("\n"):
        if username in line:
            found_username = True
            # update realm if different
            krb_realm = line.split("@")[-1]
        line = line.split()
        if len(line) == 5:
            try:
                expire_date = datetime.strptime(line[2] + " " + line[3],'%m/%d/%Y %H:%M:%S')
            except (ValueError) as e:
                pass
    
    if (found_username) and (expire_date != None) and (expire_date > datetime.today()) and (not force):
        return True
    else:
        # create 'kdestory" command
        cmd = "kdestroy"
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(None)[0].decode("utf-8") 

        # create "kinit" command
        cmd = "kinit " + username + "@" + krb_realm
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(get_password("Password for " + username + "@" + krb_realm + ": ").encode('utf-8'))[0].decode("utf-8").strip()
        # if result
        if result.strip() != "Password for " + username + "@" + krb_realm + ":":
            print("Failed to obtained Kerberos Ticket.")
            # result
            print(result)
            return False
        # ticket got successfully
        else:
            print("Successfully obtained Kerberos Ticket.\n")
            return True

success = authenticate_kerberos(force=False)


# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()


## Store level data
query = """ select period_description_short,
 market_display_name_mlr,
 mc_subsegment_c,
 sum(units) as units,
 sum(dollar) as dollars,

CASE
    WHEN mc_package_size_c = '1pk' THEN 'Singles'
    WHEN mc_package_size_c in ('2pk','3pk','4pk') THEN 'Two_to_Four_Packs'
    WHEN mc_package_size_c in ('6pk','8pk') THEN 'Six_to_Eight_Packs'
    WHEN mc_package_size_c in ('9pk', '10pk','12pk') THEN 'Nine_to_FifteenTwelveOz_Packs'
    WHEN mc_package_size_c in ('15pk') and mc_ounce_c in ('12oz') THEN 'Nine_to_FifteenTwelveOz_Packs'
    WHEN mc_package_size_c in ('15pk') and mc_ounce_c in ('16oz','19.2oz','22oz','25oz','24oz','18oz') THEN 'FiftenSixteenOz_to_Twenty_Packs'
    WHEN mc_package_size_c in ('18pk','20pk') THEN 'FiftenSixteenOz_to_Twenty_Packs'
    ELSE 'TwentyFour_Plus_Packs'
END AS pack_group,
CASE
    WHEN mc_subsegment_c = 'wine' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c = 'hard seltzer' THEN 'hard seltzer'
    WHEN mc_subsegment_c = 'alternative' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'coolers' THEN 'coolers, fmbs, wine, alternative'
    WHEN mc_subsegment_c = 'fmb' AND mc_mega_style_c IN ('fruit/spice beer','porter/stout/brown','not applicable') THEN 'coolers, fmbs, wine, alternative'
    ELSE mc_subsegment_c
END AS mc_subsegment_c2

from

(select z.market_display_name_mlr, z.units, z.dollar, z.period_description_short , c.mc_subsegment_c , c.mc_ounce_c, c.mc_package_size_c , c.mc_mega_style_c 
 from 
 (select market_display_name_mlr, units, dollar, upc, period_description_short

 from (select distinct * from core.acn_stc_market) a
 where market_display_name_mlr = 'ALBSCO Dal & Ft Wth TA') z 
 
 left join  (select mc_subsegment_c, upc,mc_ounce_c, mc_package_size_c, mc_mega_style_c from (select 
 distinct * from core.acn_product_dim) f ) c 
 on z.upc = c.upc   
 where mc_package_size_c not in ('keg','rem pack size') ) t
 group by market_display_name_mlr , period_description_short , mc_subsegment_c , mc_package_size_c, mc_subsegment_c2, pack_group

  """

#pull in data from Impala, create dfs
modeling_data = impala_select(cnxn,query)

#create month, quarter, season flags
modeling_data['date'] = pd.to_datetime(modeling_data['period_description_short'])
modeling_data['mc_subsegment_c'] = modeling_data['mc_subsegment_c2']

#create grouping variable for easier looping in modeling phase:
modeling_data['store_name2'] =  modeling_data['market_display_name_mlr'].astype(str).str.replace(u'\xa0', '')
modeling_data['store_name2'] =  modeling_data['store_name2'].astype(str).str.replace(' ', '_')
modeling_data['mc_subsegment_c2'] =  modeling_data['mc_subsegment_c'].astype(str).str.replace(' ', '_')
modeling_data['model_group'] = modeling_data['store_name2']+'_'+modeling_data['mc_subsegment_c2']+'_'+modeling_data['pack_group']

#Hard-code COVID start-date
modeling_data['stay_home_start_date'] = '2020-03-01'
modeling_data['stay_home_start_date'] = pd.to_datetime(modeling_data['stay_home_start_date'])


model_group_list = list(modeling_data['model_group'].unique())


def modeling(model_group_list):
    for i in model_group_list:
        df = modeling_data[(modeling_data['model_group'] == i)].sort_values(by='date')

            #df = df.merge(covid_dates, how= 'left', on= 'state')
            #skip modeling_groups that dropped distribution or sales well before COVID happened so forecasts aren't created for 
            # pre-COVID weeks on those groups
        if max(df['date']) < max(df['stay_home_start_date']):
    #        df = df[['market_display_name_mlr', 'mc_subsegment_c','pack_group']].drop_duplicates()
    #        for row in df.itertuples(index=True, name='Pandas'):
    #            print(row)
    #            cursor.execute("INSERT INTO patricia_scully.covid_model_skips_albertsons(market_display_name_mlr, mc_subsegment_c, pack_group) values(?,?,?)", (getattr(row, 'market_display_name_mlr')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
                print(i + ' model group was skipped.')

        if len(df) < 52:
     #       df = df[['market_display_name_mlr', 'mc_subsegment_c','pack_group']].drop_duplicates()

    #        for row in df.itertuples(index=True, name='Pandas'):
    #            print(row)
     #           cursor.execute("INSERT INTO patricia_scully.covid_model_skips_albertsons(market_display_name_mlr, mc_subsegment_c, pack_group) values(?,?,?)", (getattr(row, 'market_display_name_mlr')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
                print(i + ' model group was skipped.') 


        else:
            df = df[(df['date']<= max(df['stay_home_start_date']))]

            total_units = df['units'].sum()
            total_revenue = df['dollars'].sum()
            avg_price = total_revenue/total_units
            df = df[['date','market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group', 'units','dollars']]
            market_display_name_mlr = df['market_display_name_mlr'].unique()
            mc_subsegment_c = df['mc_subsegment_c'].unique()
            pack_group = df['pack_group'].unique()
            model_group = df['model_group'].unique()
            df = df.groupby(['date','market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})
            df = df[['date','units']].sort_values(by= 'date')

            df = df.sort_values(by='date')
            max_date = max(df['date'])
            min_date = min(df['date'])
            df = df.set_index('date')

            #fill in missing dates
            df = df.resample("W").ffill()

            #create auto arima model
            fit2 = pm.auto_arima(df, 
                            m=52,
                            # D = 1,
                            # error_action='ignore',
                             suppress_warnings=True,
                             stepwise=True,
                             seasonal_test = 'ch',
                             method='nm',
                           #  random_state = 11, 
                             maxiter = 5, 
                           #  start_p=1, start_q=1,
                           #          test='adf',
                                     max_p=3, max_q=3, start_P=0,
                                 seasonal=True
                           #          d=None, trace=True, stepwise=True
                            )

            fit2.fit(df)

         #cv = model_selection.SlidingWindowForecastCV(step=20, h=1)
         #model_cv_scores = model_selection.cross_val_score(
         #    fit2, df, scoring='smape', cv=cv, verbose=1, error_score='raise')

         #m1_average_error = np.average(model_cv_scores)
            #create blank dataframe of dates from end of current data through the rest of the year
            index = pd.date_range(start= max_date, periods=42, freq='W-SAT')
            df_forecasts = pd.DataFrame(index=index)

            #fit2.summary()
            #create predictions for rest of the year 
            forecasts = fit2.predict(n_periods=len(df_forecasts))

            df_forecasts['forecasted_units'] = forecasts
            df_forecasts['market_display_name_mlr'] = market_display_name_mlr[0]
            df_forecasts['mc_subsegment_c'] = mc_subsegment_c[0]
            df_forecasts['pack_group'] = pack_group[0]
            df_forecasts['model_group'] = model_group[0]
            df_forecasts.index.names = ['date']
            df_forecasts.reset_index(inplace=True)
            df_forecasts['forecasted_revenue'] =  df_forecasts['forecasted_units'].astype(float)*avg_price.astype(float)

            df_forecasts = df_forecasts[['date', 'market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units','forecasted_revenue']]

            df_forecasts = df_forecasts.rename(columns={'date': 'week_ending_date'})
            df_forecasts['week_ending_date'] = df_forecasts['week_ending_date'].astype(str)
            df_forecasts['forecasted_units'] = round(df_forecasts['forecasted_units'])
            df_forecasts['forecasted_units'] = df_forecasts['forecasted_units'].astype(str)
            df_forecasts['forecasted_revenue'] = round(df_forecasts['forecasted_revenue'])
            df_forecasts['forecasted_revenue'] = df_forecasts['forecasted_revenue'].astype(str)
            #because error is being thrown and stopping the modeling process with the cv score step currently, 
            #hard-code cv_score as 'NA' for the time being

            #df_forecasts['cv_score'] = m1_average_error
            #df_forecasts['cv_score'] = round(df_forecasts['cv_score'])
            #df_forecasts['cv_score'] = df_forecasts['cv_score'].astype(str)

            df_forecasts['cv_score'] = 'NA'

            #insert dataframe into HUE
            #for row in df_forecasts.itertuples(index=True, name='Pandas'):
            #    print(row)
            #    cursor.execute("INSERT INTO patricia_scully.covid_forecasts_albertsons(week_ending_date,market_display_name_mlr,mc_subsegment_c,pack_group,model_group,forecasted_units,forecasted_revenue,cv_score) values(?,?,?,?,?,?,?,?)", (getattr(row, 'week_ending_date')),(getattr(row, 'market_display_name_mlr')),(getattr(row, 'mc_subsegment_c')), (getattr(row, 'pack_group')), (getattr(row, 'model_group')), (getattr(row, 'forecasted_units')), (getattr(row, 'forecasted_revenue')), (getattr(row, 'cv_score')))
            #delete table if table exists
            cur = cnxn.cursor()
            #string = "drop table if exists patricia_scully.covid_actuals_albertsons"
            #cur.execute(string)
            #cnxn.commit()
            #create new table
            #string ="""create table patricia_scully.covid_actuals (week_ending_date STRING, model_group STRING, units STRING, dollars STRING); """
            #cur.execute(string)
            #cnxn.commit()
            ##insert actuals into table
            #for row in actuals_data.itertuples(index=True, name='Pandas'):
            #    print(row)
            #    cursor.execute("INSERT INTO patricia_scully.covid_actuals(week_ending_date,model_group,units, dollars) values(?,?,?,?)", (getattr(row, 'week_ending_date')), (getattr(row, 'model_group')), (getattr(row, 'units')), (getattr(row, 'dollars')))
            #  
            
            
#            df_forecasts_str = df_forecasts.astype(str) 
#            values=("""('"""+df_forecasts_str[df_forecasts_str.columns[0]]+"""','"""+df_forecasts_str[df_forecasts_str.columns[1]]+"""','"""+df_forecasts_str[df_forecasts_str.columns[2]]+"""','""" +df_forecasts_str[df_forecasts_str.columns[3]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[4]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[5]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[6]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[7]]+ """')""").str.cat(sep=',')
#            cnxn.cursor().execute("""insert into table patricia_scully.covid_forecasts_albertsons values """ + values)

            forecasts_dict[i] = df_forecasts

            print(i + ' model group has finished successfully.')


def modeling_mp(model_group_list):
    cores_available = mp.cpu_count()-8
    if len(model_group_list) < cores_available:
        chunks = [model_group_list[i::len(model_group_list)] for i in range(len(model_group_list))]
    
        pool = mp.Pool(processes = len(model_group_list))

        pool.map(modeling, chunks)
        
        pool.close()
    
    else:
    
        chunks = [model_group_list[i::cores_available] for i in range(cores_available)]
    
        pool = mp.Pool(processes = cores_available)

        pool.map(modeling, chunks)
        
        pool.close()




# =============================================================================
#  Serial Run
# =============================================================================


#forecasts_dict = {}
#
#start = datetime.now()
#
#modeling(model_group_list)
#
#end = datetime.now()
#
#run_time_serial = end-start
#
#print(run_time_serial)



# =============================================================================
#  Multiprocessing 1
# =============================================================================


forecasts_dict = {}

start = datetime.now()

modeling_mp(model_group_list)

end = datetime.now()

run_time_multiprocessing = end-start

print(run_time_multiprocessing)

## =============================================================================
## Multiprocessing 2
## =============================================================================
#
#
#def modeling2(model_group_list):
#    for i in model_group_list:
#        df = modeling_data[(modeling_data['model_group'] == i)].sort_values(by='date')
#
#            #df = df.merge(covid_dates, how= 'left', on= 'state')
#            #skip modeling_groups that dropped distribution or sales well before COVID happened so forecasts aren't created for 
#            # pre-COVID weeks on those groups
#        if max(df['date']) < max(df['stay_home_start_date']):
#            df = df[['market_display_name_mlr', 'mc_subsegment_c','pack_group']].drop_duplicates()
#            for row in df.itertuples(index=True, name='Pandas'):
#                print(row)
#                cursor.execute("INSERT INTO patricia_scully.covid_model_skips_albertsons(market_display_name_mlr, mc_subsegment_c, pack_group) values(?,?,?)", (getattr(row, 'market_display_name_mlr')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
#                print(i + ' model group was skipped.')
#
#        if len(df) < 52:
#            df = df[['market_display_name_mlr', 'mc_subsegment_c','pack_group']].drop_duplicates()
#
#            for row in df.itertuples(index=True, name='Pandas'):
#                print(row)
#                cursor.execute("INSERT INTO patricia_scully.covid_model_skips_albertsons(market_display_name_mlr, mc_subsegment_c, pack_group) values(?,?,?)", (getattr(row, 'market_display_name_mlr')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
#                print(i + ' model group was skipped.') 
#
#
#        else:
#            df = df[(df['date']<= max(df['stay_home_start_date']))]
#
#            total_units = df['units'].sum()
#            total_revenue = df['dollars'].sum()
#            avg_price = total_revenue/total_units
#            df = df[['date','market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group', 'units','dollars']]
#            market_display_name_mlr = df['market_display_name_mlr'].unique()
#            mc_subsegment_c = df['mc_subsegment_c'].unique()
#            pack_group = df['pack_group'].unique()
#            model_group = df['model_group'].unique()
#            df = df.groupby(['date','market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})
#            df = df[['date','units']].sort_values(by= 'date')
#
#            df = df.sort_values(by='date')
#            max_date = max(df['date'])
#            min_date = min(df['date'])
#            df = df.set_index('date')
#
#            #fill in missing dates
#            df = df.resample("W").ffill()
#
#            #create auto arima model
#            fit2 = pm.auto_arima(df, 
#                            m=52,
#                            # D = 1,
#                            # error_action='ignore',
#                             suppress_warnings=True,
#                             stepwise=True,
#                             seasonal_test = 'ch',
#                             method='nm',
#                           #  random_state = 11, 
#                             maxiter = 5, 
#                           #  start_p=1, start_q=1,
#                           #          test='adf',
#                                     max_p=3, max_q=3, start_P=0,
#                                 seasonal=True
#                           #          d=None, trace=True, stepwise=True
#                            )
#
#            fit2.fit(df)
#
#         #cv = model_selection.SlidingWindowForecastCV(step=20, h=1)
#         #model_cv_scores = model_selection.cross_val_score(
#         #    fit2, df, scoring='smape', cv=cv, verbose=1, error_score='raise')
#
#         #m1_average_error = np.average(model_cv_scores)
#            #create blank dataframe of dates from end of current data through the rest of the year
#            index = pd.date_range(start= max_date, periods=42, freq='W-SAT')
#            df_forecasts = pd.DataFrame(index=index)
#
#            #fit2.summary()
#            #create predictions for rest of the year 
#            forecasts = fit2.predict(n_periods=len(df_forecasts))
#
#            df_forecasts['forecasted_units'] = forecasts
#            df_forecasts['market_display_name_mlr'] = market_display_name_mlr[0]
#            df_forecasts['mc_subsegment_c'] = mc_subsegment_c[0]
#            df_forecasts['pack_group'] = pack_group[0]
#            df_forecasts['model_group'] = model_group[0]
#            df_forecasts.index.names = ['date']
#            df_forecasts.reset_index(inplace=True)
#            df_forecasts['forecasted_revenue'] =  df_forecasts['forecasted_units'].astype(float)*avg_price.astype(float)
#
#            df_forecasts = df_forecasts[['date', 'market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units','forecasted_revenue']]
#
#            df_forecasts = df_forecasts.rename(columns={'date': 'week_ending_date'})
#            df_forecasts['week_ending_date'] = df_forecasts['week_ending_date'].astype(str)
#            df_forecasts['forecasted_units'] = round(df_forecasts['forecasted_units'])
#            df_forecasts['forecasted_units'] = df_forecasts['forecasted_units'].astype(str)
#            df_forecasts['forecasted_revenue'] = round(df_forecasts['forecasted_revenue'])
#            df_forecasts['forecasted_revenue'] = df_forecasts['forecasted_revenue'].astype(str)
#            #because error is being thrown and stopping the modeling process with the cv score step currently, 
#            #hard-code cv_score as 'NA' for the time being
#
#            #df_forecasts['cv_score'] = m1_average_error
#            #df_forecasts['cv_score'] = round(df_forecasts['cv_score'])
#            #df_forecasts['cv_score'] = df_forecasts['cv_score'].astype(str)
#
#            df_forecasts['cv_score'] = 'NA'
#
#            #insert dataframe into HUE
#            #for row in df_forecasts.itertuples(index=True, name='Pandas'):
#            #    print(row)
#            #    cursor.execute("INSERT INTO patricia_scully.covid_forecasts_albertsons(week_ending_date,market_display_name_mlr,mc_subsegment_c,pack_group,model_group,forecasted_units,forecasted_revenue,cv_score) values(?,?,?,?,?,?,?,?)", (getattr(row, 'week_ending_date')),(getattr(row, 'market_display_name_mlr')),(getattr(row, 'mc_subsegment_c')), (getattr(row, 'pack_group')), (getattr(row, 'model_group')), (getattr(row, 'forecasted_units')), (getattr(row, 'forecasted_revenue')), (getattr(row, 'cv_score')))
#            #delete table if table exists
#            cur = cnxn.cursor()
#            #string = "drop table if exists patricia_scully.covid_actuals_albertsons"
#            #cur.execute(string)
#            #cnxn.commit()
#            #create new table
#            #string ="""create table patricia_scully.covid_actuals (week_ending_date STRING, model_group STRING, units STRING, dollars STRING); """
#            #cur.execute(string)
#            #cnxn.commit()
#            ##insert actuals into table
#            #for row in actuals_data.itertuples(index=True, name='Pandas'):
#            #    print(row)
#            #    cursor.execute("INSERT INTO patricia_scully.covid_actuals(week_ending_date,model_group,units, dollars) values(?,?,?,?)", (getattr(row, 'week_ending_date')), (getattr(row, 'model_group')), (getattr(row, 'units')), (getattr(row, 'dollars')))
#            #  
#            
#            
##            df_forecasts_str = df_forecasts.astype(str) 
##            values=("""('"""+df_forecasts_str[df_forecasts_str.columns[0]]+"""','"""+df_forecasts_str[df_forecasts_str.columns[1]]+"""','"""+df_forecasts_str[df_forecasts_str.columns[2]]+"""','""" +df_forecasts_str[df_forecasts_str.columns[3]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[4]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[5]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[6]]+ """','""" +df_forecasts_str[df_forecasts_str.columns[7]]+ """')""").str.cat(sep=',')
##            cnxn.cursor().execute("""insert into table patricia_scully.covid_forecasts_albertsons values """ + values)
#
#            print(i + ' model group has finished successfully.')
#    
#            return(df_forecasts)
#            
#            
#start = datetime.now()
#
#NUM_CORES = mp.cpu_count()-8
#
#df_chunks = [model_group_list[i::NUM_CORES] for i in range(NUM_CORES)]
#
## use a pool to spawn multiple proecsses
#with mp.Pool(NUM_CORES) as pool:
#
#  # concatenate all processed chunks together.
#  # process_df_function is the function you defined in the previous block
#  processed_df = pd.concat(pool.map(modeling2, df_chunks), ignore_index=True)
#
#end = datetime.now()
#
#run_time_multiprocessing_2 = end-start
#
#print(run_time_multiprocessing_2)
#
#
#
## =============================================================================
## Multiprocesssing 3
## =============================================================================
#
#
##def processInput(i):
##        return i * i
#
#start = datetime.now()
#
#
#
#if __name__ == '__main__':
#
#    # what are your inputs, and what operation do you want to
#    # perform on each input. For example...
# #   inputs = model_group_list
#    #  removing processes argument makes the code run on all available cores
#    cores_available = mp.cpu_count()-8
#    pool = mp.Pool(processes=cores_available)
#    
#    chunks = [model_group_list[i::cores_available] for i in range(cores_available)]    
#    
#    results = pool.map(modeling2, chunks)
#    print(results)
#
#end = datetime.now()
#
#run_time = end-start
#
#print(run_time)
#
#
#
#
## =============================================================================
## Dask Processing
## =============================================================================
#
#def modeling3(i):
#    df = modeling_data[(modeling_data['model_group'] == i)].sort_values(by='date')
#        #df = df.merge(covid_dates, how= 'left', on= 'state')
#        #skip modeling_groups that dropped distribution or sales well before COVID happened so forecasts aren't created for 
#        # pre-COVID weeks on those groups
#    if max(df['date']) < max(df['stay_home_start_date']):
# #       df = df[['market_display_name_mlr', 'mc_subsegment_c','pack_group']].drop_duplicates()
# #       for row in df.itertuples(index=True, name='Pandas'):
# #           print(row)
# #           cursor.execute("INSERT INTO patricia_scully.covid_model_skips_albertsons(market_display_name_mlr, mc_subsegment_c, pack_group) values(?,?,?)", (getattr(row, 'market_display_name_mlr')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
#            print(i + ' model group was skipped.')
#    if len(df) < 52:
# #       df = df[['market_display_name_mlr', 'mc_subsegment_c','pack_group']].drop_duplicates()
# #       for row in df.itertuples(index=True, name='Pandas'):
# #           print(row)
# #           cursor.execute("INSERT INTO patricia_scully.covid_model_skips_albertsons(market_display_name_mlr, mc_subsegment_c, pack_group) values(?,?,?)", (getattr(row, 'market_display_name_mlr')), (getattr(row, 'mc_subsegment_c')),(getattr(row, 'pack_group')))
#            print(i + ' model group was skipped.') 
#    else:
#        df = df[(df['date']<= max(df['stay_home_start_date']))]
#        total_units = df['units'].sum()
#        total_revenue = df['dollars'].sum()
#        avg_price = total_revenue/total_units
#        df = df[['date','market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group', 'units','dollars']]
#        market_display_name_mlr = df['market_display_name_mlr'].unique()
#        mc_subsegment_c = df['mc_subsegment_c'].unique()
#        pack_group = df['pack_group'].unique()
#        model_group = df['model_group'].unique()
#        df = df.groupby(['date','market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group'],as_index=False).aggregate({'units': 'sum','dollars':'sum'})
#        df = df[['date','units']].sort_values(by= 'date')
#        df = df.sort_values(by='date')
#        max_date = max(df['date'])
#        min_date = min(df['date'])
#        df = df.set_index('date')
#        #fill in missing dates
#        df = df.resample("W").ffill()
#        #create auto arima model
#        fit2 = pm.auto_arima(df, 
#                        m=52,
#                        # D = 1,
#                        # error_action='ignore',
#                         suppress_warnings=True,
#                         stepwise=True,
#                         seasonal_test = 'ch',
#                         method='nm',
#                       #  random_state = 11, 
#                         maxiter = 5, 
#                       #  start_p=1, start_q=1,
#                       #          test='adf',
#                                 max_p=3, max_q=3, start_P=0,
#                             seasonal=True
#                       #          d=None, trace=True, stepwise=True
#                        )
#        fit2.fit(df)
#     #cv = model_selection.SlidingWindowForecastCV(step=20, h=1)
#     #model_cv_scores = model_selection.cross_val_score(
#     #    fit2, df, scoring='smape', cv=cv, verbose=1, error_score='raise')
#     #m1_average_error = np.average(model_cv_scores)
#        #create blank dataframe of dates from end of current data through the rest of the year
#        index = pd.date_range(start= max_date, periods=42, freq='W-SAT')
#        df_forecasts = pd.DataFrame(index=index)
#        #fit2.summary()
#        #create predictions for rest of the year 
#        forecasts = fit2.predict(n_periods=len(df_forecasts))
#        df_forecasts['forecasted_units'] = forecasts
#        df_forecasts['market_display_name_mlr'] = market_display_name_mlr[0]
#        df_forecasts['mc_subsegment_c'] = mc_subsegment_c[0]
#        df_forecasts['pack_group'] = pack_group[0]
#        df_forecasts['model_group'] = model_group[0]
#        df_forecasts.index.names = ['date']
#        df_forecasts.reset_index(inplace=True)
#        df_forecasts['forecasted_revenue'] =  df_forecasts['forecasted_units'].astype(float)*avg_price.astype(float)
#        df_forecasts = df_forecasts[['date', 'market_display_name_mlr', 'mc_subsegment_c', 'pack_group', 'model_group', 'forecasted_units','forecasted_revenue']]
#        df_forecasts = df_forecasts.rename(columns={'date': 'week_ending_date'})
#        df_forecasts['week_ending_date'] = df_forecasts['week_ending_date'].astype(str)
#        df_forecasts['forecasted_units'] = round(df_forecasts['forecasted_units'])
#        df_forecasts['forecasted_units'] = df_forecasts['forecasted_units'].astype(str)
#        df_forecasts['forecasted_revenue'] = round(df_forecasts['forecasted_revenue'])
#        df_forecasts['forecasted_revenue'] = df_forecasts['forecasted_revenue'].astype(str)
#        #because error is being thrown and stopping the modeling process with the cv score step currently, 
#        #hard-code cv_score as 'NA' for the time being
#        df_forecasts['cv_score'] = 'NA'
#        print(i + ' model group has finished successfully.')
#
#        return(df_forecasts)
#
#start = datetime.now()
#
## Example loop will add results to a list and calculate total
#results = []
#for i in model_group_list:
#    # Call normal function with dask delayed decorator
#    x = delayed(modeling3)(i)
#    results.append(x)
##    results[i] = x
#
##forecasts = pd.concat(forecasts_dict)
##forecasts = forecasts.reset_index(drop = True)
##final_result = total.compute()
##final_result = results.compute()
#final_result = dask.compute(*results)
#
## Calculate time taken and print results
#end = datetime.now()
#run_time = end-start
#
#
## =============================================================================
##Conccurrent Futures
## =============================================================================
#
#start = datetime.now()
#
#with concurrent.futures.ProcessPoolExecutor() as executor:
#    executor.map(modeling3, model_group_list)
#    
#end = datetime.now()
#run_time = end-start
